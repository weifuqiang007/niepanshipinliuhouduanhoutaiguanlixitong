/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : niepan

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 09/03/2023 11:46:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for threshold_old
-- ----------------------------
DROP TABLE IF EXISTS `threshold_old`;
CREATE TABLE `threshold_old`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `em_obj_type` int(11) NULL DEFAULT NULL COMMENT '物品的类型',
  `item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物品的名称',
  `threshole` int(11) NULL DEFAULT NULL COMMENT '阈值',
  `person_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作用户',
  `create_time` datetime(6) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(6) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 177 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of threshold_old
-- ----------------------------
INSERT INTO `threshold_old` VALUES (1, 4, '雨伞', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (2, 5, '手机', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (3, 6, '笔记本', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (4, 7, '充电宝', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (5, 3, '枪支', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (6, 13, '烟花爆竹', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (7, 12, '喷雾喷灌', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (8, 1, '刀具', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (9, 23, '斧头', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (10, 33, '剪刀', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (11, 30, '打火机油', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (12, 31, '指甲油', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (13, 2, '瓶装液体', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (14, 28, '玻璃杯', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (15, 27, '保温杯', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (16, 29, '塑料瓶', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (17, 17, '手铐', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (18, 15, '警棍', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (19, 16, '指虎', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (20, 14, '打火机', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (21, 32, '工具', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (22, 34, '电子产品', 80, NULL, '2023-02-14 11:17:30.000000', '2023-02-14 11:17:30.000000');
INSERT INTO `threshold_old` VALUES (133, 3, '枪支', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (134, 13, '烟花爆竹', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (135, 12, '喷雾喷灌', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (136, 1, '刀具', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (137, 23, '斧头', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (138, 33, '剪刀', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (139, 30, '打火机油', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (140, 31, '指甲油', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (141, 2, '瓶装液体', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (142, 28, '玻璃杯', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (143, 27, '保温杯', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (144, 29, '塑料瓶', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (145, 17, '手铐', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (146, 15, '警棍', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (147, 16, '指虎', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (148, 14, '打火机', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (149, 32, '工具', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (150, 34, '电子产品', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (151, 5, '手机', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (152, 7, '充电宝', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (153, 6, '笔记本', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (154, 4, '雨伞', 80, 'root', NULL, '2023-03-09 10:15:11.000000');
INSERT INTO `threshold_old` VALUES (155, 3, '枪支', 30, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (156, 13, '烟花爆竹', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (157, 12, '喷雾喷灌', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (158, 1, '刀具', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (159, 23, '斧头', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (160, 33, '剪刀', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (161, 30, '打火机油', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (162, 31, '指甲油', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (163, 2, '瓶装液体', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (164, 28, '玻璃杯', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (165, 27, '保温杯', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (166, 29, '塑料瓶', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (167, 17, '手铐', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (168, 15, '警棍', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (169, 16, '指虎', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (170, 14, '打火机', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (171, 32, '工具', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (172, 34, '电子产品', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (173, 5, '手机', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (174, 7, '充电宝', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (175, 6, '笔记本', 80, 'root', NULL, '2023-03-09 10:15:25.000000');
INSERT INTO `threshold_old` VALUES (176, 4, '雨伞', 80, 'root', NULL, '2023-03-09 10:15:25.000000');

SET FOREIGN_KEY_CHECKS = 1;
