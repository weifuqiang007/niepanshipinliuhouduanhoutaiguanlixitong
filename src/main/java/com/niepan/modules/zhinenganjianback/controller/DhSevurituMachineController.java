//package com.niepan.modules.zhinenganjianback.controller;
//
//import com.niepan.common.utils.R.ApiResponse;
//import com.niepan.common.utils.R.Result;
//import com.niepan.common.utils.json.JsonToFile;
//import com.niepan.common.utils.logs.LogUtils;
//import com.niepan.modules.zhinenganjianback.VO.DhImgVO;
//import com.niepan.modules.zhinenganjianback.VO.DhStatusVO;
//import com.niepan.modules.zhinenganjianback.dto.GetHeartInfoDto;
//import com.niepan.modules.zhinenganjianback.enums.AtxStatusEnum;
//import com.niepan.modules.zhinenganjianback.enums.DhStatusEnum;
//import com.niepan.modules.zhinenganjianback.model.DhHeartBeat;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
//import com.niepan.modules.zhinenganjianback.service.DhSecurityMachineService;
//import com.niepan.modules.zhinenganjianback.service.SecurityMachineService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.*;
//
//import java.sql.Timestamp;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * 大华安检机接口
// * @author: liuchenyu
// * @date: 2023/5/18
// */
//@RestController
//@RequestMapping("/dh/api/securitydevice")
//public class DhSevurituMachineController {
//
//    @Value("${file.picPath}")
//    private String picPath;
//
//    @Autowired
//    private DhSecurityMachineService dhSecurityMachineService;
//
//    @Autowired
//    private SecurityMachineService securityMachineService;
//
//    // 实现接受图片的请求
//    @RequestMapping(value = "/img_send", method = RequestMethod.POST)
//    public ApiResponse img_send(@RequestBody DhImgVO dhImgVO){
//
//        String substring = dhImgVO.getChecktime().substring(0, dhImgVO.getChecktime().lastIndexOf(" "));
//        Timestamp timestamp = Timestamp.valueOf(substring);
//        long time = timestamp.getTime();
//        String picName = dhImgVO.getDeviceid() + "@#" + time + ".jpg";
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//        String datePath = sdf.format(new Date());//20230215
//        //存放图片的路径
//        String filePath = picPath + dhImgVO.getDeviceid() + "/" + datePath + "/";
//
//        //图片存储到本地
//        JsonToFile.stringToJpg(dhImgVO.getTopimagedata(),filePath+picName);
//
//        //日志记录
//        LogUtils.info("photo path is " + filePath + picName);
//        LogUtils.info("photo info is " + dhImgVO.getChecktime());
//
//        //将要插入数据库的数据封装到对象中
//        SecurityMachinePicture securityMachinePicture = new SecurityMachinePicture();
//        securityMachinePicture.setName(picName);
//        securityMachinePicture.setDevice_sn(dhImgVO.getDeviceid());
//        securityMachinePicture.setLine_code("1");//安天下安检机没有信道，默认为1
//        securityMachinePicture.setTime(dhImgVO.getChecktime());
//
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
//        String date = df.format(new Date());//2023-02-17 13:49:08
//        securityMachinePicture.setCurrentTime(date);
//
//        //数据库插入 并推送给大华设备
//        Boolean res = dhSecurityMachineService.getImgInfo(securityMachinePicture,dhImgVO.getTopimagedata());
//        if (res != null && res){
//            LogUtils.info("图片保存成功");
//            return Result.success("图片保存成功");
//        }
//        LogUtils.error("保存图片失败" + picName);
//        return Result.error("保存图片失败");
//    }
//
//    // 安检机状态和故障信息上报
//    @RequestMapping(value = "/status_send", method = RequestMethod.POST)
//    public ApiResponse getStatus(@RequestBody DhStatusVO dhStatusVO){
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        if (dhStatusVO.getErrortype().equals("0")){
//            LogUtils.info("安检机状态正常");
//            LogUtils.info(formatter.format(new Date()) + "@#" + dhStatusVO.getDeviceid() +"@#"+
//                    dhStatusVO.getErrortype() + "@#" + DhStatusEnum.getValue(dhStatusVO.getErrortype()));
//        }
//        else{
//            LogUtils.error("安检机状态异常");
//            LogUtils.error(formatter.format(new Date()) + "@#" + dhStatusVO.getDeviceid() +"@#"+
//                    dhStatusVO.getErrortype() + "@#" + DhStatusEnum.getValue(dhStatusVO.getErrortype()));
//        }
//
//        SecurityMachineStatus securityMachineStatus = new SecurityMachineStatus();
//        securityMachineStatus.setDevice_sn(dhStatusVO.getDeviceid());
//        securityMachineStatus.setLine_code("1");//安天下安检机没有信道，默认为1
//        securityMachineStatus.setStatus_code(dhStatusVO.getErrortype().toString());
//        securityMachineStatus.setStatus_desc(AtxStatusEnum.getValue(dhStatusVO.getErrortype()));
//        securityMachineStatus.setFault_level(dhStatusVO.getDevicestatus().toString());
//        //传来的时间是yyyy-MM-dd HH:mm:ss SSS 将其转化为时间戳
//        String substring = dhStatusVO.getErrortime().substring(0, dhStatusVO.getErrortime().lastIndexOf(" "));
//        Timestamp timestamp = Timestamp.valueOf(substring);
//        long time = timestamp.getTime();
//        securityMachineStatus.setTime(time);
//        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
//        String date = df.format(new Date());//20230215
//        securityMachineStatus.setMsgtime(date);
//
//        String status = dhSecurityMachineService.SecurityMachineStatus(securityMachineStatus);
//
//        return Result.success(status);
//    }
//
//    SimpleDateFormat num = new SimpleDateFormat("HH:mm:ss");
//    String formatOld = num.format(new Date());
//
//    // 安检机心跳
//    @RequestMapping(value = "/heart", method = RequestMethod.POST)
//    public ApiResponse getHeart(@RequestBody DhHeartBeat dhHeartBeat){
//
//        System.out.println(dhHeartBeat.getDatatime());
//        System.out.println(dhHeartBeat.getProtocolversion());
//        Date date = new Date();
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(formatter.format(date));
//
//        LogUtils.info("heart beat time is :"+formatter.format(date) + "deviceId is :"+dhHeartBeat.getDeviceid() +
//                "ProtocolVersion is :"+ dhHeartBeat.getProtocolversion());
//
//        try {
//
//            String format = num.format(new Date());
//
//            Date dateOld=num.parse(formatOld);
//            Date date2=num.parse(format);
//
//            if (date2.getTime() - dateOld.getTime() > 10000){
//                GetHeartInfoDto getHeartInfoDto = new GetHeartInfoDto();
//                getHeartInfoDto.setDevice_sn(dhHeartBeat.getDeviceid());
//                securityMachineService.getHeart(getHeartInfoDto);
//                formatOld = format;
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        return Result.success();
//    }
//
//    @RequestMapping(value = "/heart1")
//    public ApiResponse getHeart1(Object... args){
//        for (Object arg : args) {
//            System.out.println(arg);
//            System.out.println(arg.getClass().toString());
//        }
//        return Result.success();
//    }
//}
