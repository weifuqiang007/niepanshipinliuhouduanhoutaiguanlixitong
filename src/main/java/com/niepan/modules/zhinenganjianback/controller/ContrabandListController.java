package com.niepan.modules.zhinenganjianback.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.common.utils.aesUtil.AesEncryptUtil;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.common.websocket.server.WarningWebSocketServer;
import com.niepan.modules.zhinenganjianback.service.ContrabandListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author:liuchenyu
 * @data:2023/02/27
 * 智能安检模块
 *
 */
@RestController
@RequestMapping("/ContrabandList")
public class ContrabandListController {

    @Autowired
    ContrabandListService contrabandListService;

    @Autowired
    WarningWebSocketServer warningWebSocketServer;

    /**
     * websocket 的测试方法
     * @throws IOException
     */
    @RequestMapping("/test")
    public void test1() throws IOException {
        warningWebSocketServer.sendMessage("1111111");
    }

    /**
     * 获取所有的地区
     *
     * todo 之后修改sql SELECT * FROM `sys_user_area` a
     *                 left join area b on a.unique_code = b.unique_code
     *                 where b.unique_code in (a.unique_code)
     *                 and a.user_id = 'cbe0dd9613414421a44ff48fd424e325'
     *
     * @return
     */
    @GetMapping("/getArea")
    public ApiResponse getArea(){
        LogUtils.info("开始查询所有地区");
        List<Map<String,Object>> res = contrabandListService.getArea();
        return Result.success(res);
    }

    /**
     * 根据条件获取信息
     * @param pageSize
     * @param pageNum
     * @param uniqueCode
     * @param machineGrade 安检机标号
     * @param machineBrand 安检机品牌
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getItem")
    public ApiResponse getItem(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                            @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                            @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                            @RequestParam(value = "machineGrade",required = false) String machineGrade,
                            @RequestParam(value = "machineBrand",required = false) String machineBrand,
                            @RequestParam(value = "startTime",required = true) String startTime,
                            @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息");
//        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> res = contrabandListService.getItem(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }


    /**
     * 根据条件获取信息
     * @param pageSize
     * @param pageNum
     * @param uniqueCode
     * @param machineGrade 安检机标号
     * @param machineBrand 安检机品牌
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getSpItem")
    public ApiResponse getSpItem(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                               @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                               @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                               @RequestParam(value = "machineGrade",required = false) String machineGrade,
                               @RequestParam(value = "machineBrand",required = false) String machineBrand,
                               @RequestParam(value = "startTime",required = true) String startTime,
                               @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息");
//        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> res = contrabandListService.getSpItem(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 根据条件获取信息的统计数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getCount")
    public ApiResponse getCount(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                @RequestParam(value = "startTime",required = true) String startTime,
                                @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息的统计数");
        List<Map<String, Object>> res = contrabandListService.getCount(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 根据唯一标识查询图片的详情
     * @param pageSize
     * @param pageNum
     * @param UUID
     * @return
     */
    @GetMapping("/getDetail")
    public PageInfo getDetail(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                              @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                              @RequestParam("UUID") String UUID){
        LogUtils.info("开始根据唯一标识查询图片的详情");
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> res = contrabandListService.getDetail(UUID);
        return new PageInfo<>(res);
    }

    /**
     * 根据省市获取集散中心列表
     * @param uniqueCode
     * @return
     */
    @GetMapping("/getCentre")
    public ApiResponse getCentre(@RequestParam("uniqueCode") String uniqueCode){
        LogUtils.info("开始根据省市获取集散中心列表");
        List<Map<String,Object>> res = contrabandListService.getCentre(uniqueCode);
        return Result.success(res);
    }

    /**
     * 获取总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getTotal")
    public ApiResponse getTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                @RequestParam(value = "startTime",required = true) String startTime,
                                @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始获取总数");
        List<Map<String, Object>> res = contrabandListService.getTotal(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 获取总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getSpTotal")
    public ApiResponse getSpTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                  @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                  @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                  @RequestParam(value = "startTime",required = true) String startTime,
                                  @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始获取总数");
        List<Map<String, Object>> res = contrabandListService.getSpTotal(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 获取违禁品的总数
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getDangerTotal")
    public ApiResponse getDangerTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                      @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                      @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                      @RequestParam(value = "startTime",required = true) String startTime,
                                      @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始获取违禁品的总数");
        List<Map<String, Object>> res = contrabandListService.getDangerTotal(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    @GetMapping("/getSpDangerTotal")
    public ApiResponse getSpDangerTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                        @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                        @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                        @RequestParam(value = "startTime",required = true) String startTime,
                                        @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始获取违禁品的总数");
        List<Map<String, Object>> res = contrabandListService.getSpDangerTotal(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    @GetMapping("/getTotalAndDangerTotal")
    public ApiResponse getTotalAndDangerTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                              @RequestParam(value = "lineCode",required = false) String lineCode,
                                              @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                              @RequestParam(value = "startTime",required = true) String startTime,
                                              @RequestParam(value = "endTime",required = true) String endTime){
        Map<String,Object> map = new HashMap<>();
        List<Map<String, Object>> total = contrabandListService.getTotal(uniqueCode,lineCode,machineBrand,startTime,endTime);
        for (Map<String, Object> stringObjectMap : total) {
            map.putAll(stringObjectMap);
        }
        List<Map<String, Object>> dangerTotal = contrabandListService.getDangerTotal(uniqueCode, lineCode, machineBrand, startTime, endTime);
        for (Map<String, Object> stringObjectMap : dangerTotal) {
            map.putAll(stringObjectMap);
        }
        return Result.success(map);
    }

    @GetMapping("/getSpTotalAndDangerTotal")
    public ApiResponse getSpTotalAndDangerTotal(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                              @RequestParam(value = "lineCode",required = false) String lineCode,
                                              @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                              @RequestParam(value = "startTime",required = true) String startTime,
                                              @RequestParam(value = "endTime",required = true) String endTime){
        Map<String,Object> map = new HashMap<>();
        List<Map<String, Object>> total = contrabandListService.getSpTotal(uniqueCode,lineCode,machineBrand,startTime,endTime);
        for (Map<String, Object> stringObjectMap : total) {
            map.putAll(stringObjectMap);
        }
        List<Map<String, Object>> dangerTotal = contrabandListService.getSpDangerTotal(uniqueCode, lineCode, machineBrand, startTime, endTime);
        for (Map<String, Object> stringObjectMap : dangerTotal) {
            map.putAll(stringObjectMap);
        }
        return Result.success(map);
    }


    @GetMapping("/getSpTotalAndDangerTotalTwice")
    public ApiResponse getSpTotalAndDangerTotalTwice(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                                     @RequestParam(value = "lineCode",required = false) String lineCode,
                                                     @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                                     @RequestParam(value = "startTime",required = true) String startTime,
                                                     @RequestParam(value = "endTime",required = true) String endTime){
        Map<String,Object> map = new HashMap<>();
        List<Map<String, Object>> total = contrabandListService.getSpTotalTwice(uniqueCode,lineCode,machineBrand,startTime,endTime);
        for (Map<String, Object> stringObjectMap : total) {
            map.putAll(stringObjectMap);
        }
        List<Map<String, Object>> dangerTotal = contrabandListService.getSpDangerTotalTwice(uniqueCode, lineCode, machineBrand, startTime, endTime);
        for (Map<String, Object> stringObjectMap : dangerTotal) {
            map.putAll(stringObjectMap);
        }
        return Result.success(map);
    }

    /**
     * 获取某个违禁品类下的所有违禁品信息
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @param typeId
     * @return
     */
    @GetMapping("/getCountDetail")
    public PageInfo getCountDetail(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                   @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                   @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                   @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                   @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                   @RequestParam(value = "startTime",required = true) String startTime,
                                   @RequestParam(value = "endTime",required = true) String endTime,
                                   @RequestParam(value = "typeId",required = true)String typeId){
        LogUtils.info("开始获取某个违禁品类下的所有违禁品信息");
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String, Object>> res = contrabandListService.getCountDetail(machineGrade,machineBrand,startTime,endTime,typeId,uniqueCode);
        return new PageInfo<>(res);
    }

    /**
     * 获取某个违禁品类下的所有违禁品信息
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @param typeId
     * @return
     */
    @GetMapping("/getSpCountDetail")
    public PageInfo getSpCountDetail(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                     @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                     @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                     @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                     @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                     @RequestParam(value = "startTime",required = true) String startTime,
                                     @RequestParam(value = "endTime",required = true) String endTime,
                                     @RequestParam(value = "typeId",required = true)String typeId){
        LogUtils.info("开始获取某个违禁品类下的所有违禁品信息");
        PageHelper.startPage(pageNum, pageSize,false);
        List<Map<String, Object>> res = contrabandListService.getSpCountDetail(machineGrade,machineBrand,startTime,endTime,typeId,uniqueCode);
        return new PageInfo<>(res);
    }

    /**
     * 根据条件获取信息的统计数（统计曲线图使用）
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getCountStatistics")
    public ApiResponse getCountStatistics(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                          @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                          @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                          @RequestParam(value = "startTime",required = true) String startTime,
                                          @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息的统计数");
        List<Map<String, Object>> res = contrabandListService.getCountStatistics(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 根据条件查询每天的过包数
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getCountByDay")
    public ApiResponse getCountByDay(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                     @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                     @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                     @RequestParam(value = "startTime",required = true) String startTime,
                                     @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件查询每天的过包数");
        List<Map<String, Object>> res = contrabandListService.getCountByDay(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 根据条件查询每天的过包数
     * @param uniqueCode
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getSpCountByDay")
    public ApiResponse getSpCountByDay(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                     @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                     @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                     @RequestParam(value = "startTime",required = true) String startTime,
                                     @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件查询每天的过包数");
        List<Map<String, Object>> res = contrabandListService.getSpCountByDay(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }


    /**
     * 查询所有的图片信息
     * @param pageSize
     * @param pageNum
     * @return
     */
//    @GetMapping("/getPicture")
//    public PageInfo getPicture(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
//                               @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
//                               @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
//                               @RequestParam(value = "startTime",required = true) String startTime,
//                               @RequestParam(value = "endTime",required = true) String endTime,
//                               @RequestParam(value = "lineCode",required = false) String lineCode){
//        LogUtils.info("开始根据条件查询每天的过包数");
//        PageHelper.startPage(pageNum, pageSize);
//        List<Map<String, Object>> res = contrabandListService.getPicture(uniqueCode,startTime,endTime,lineCode);
//        return new PageInfo<>(res);
//    }

    @GetMapping("/getPicture")
    public ApiResponse getPicture(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                  @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                  @RequestParam(value = "startTime",required = true) String startTime,
                                  @RequestParam(value = "endTime",required = true) String endTime,
                                  @RequestParam(value = "lineCode",required = false) String lineCode){
        LogUtils.info("开始根据条件查询每天的过包数");
        PageHelper.startPage(pageNum, pageSize);
        List res = contrabandListService.getPicture(uniqueCode,startTime,endTime,lineCode);
        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(res);
        String resultStr= JSON.toJSONString(mapPageInfo);
        String encrypt = AesEncryptUtil.encrypt(resultStr, AesEncryptUtil.KEY,AesEncryptUtil.IV);
        return Result.success(encrypt);
    }

    /**
     * 查询所有的图片信息
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping("/getSpPicture")
    public ApiResponse getSpPicture(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                    @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                    @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                    @RequestParam(value = "startTime",required = true) String startTime,
                                    @RequestParam(value = "endTime",required = true) String endTime,
                                    @RequestParam(value = "lineCode",required = false) String lineCode,
                                    @RequestParam(value = "isDanger",required = false) Boolean isDanger){
        LogUtils.info("开始根据条件查询每天的过包数");
        PageHelper.startPage(pageNum, pageSize);

        List res = contrabandListService.getSpPicture(uniqueCode,startTime,endTime,lineCode,isDanger);
        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(res);
//        return Result.success(mapPageInfo);
        String resultStr= JSON.toJSONString(mapPageInfo);
        String encrypt = AesEncryptUtil.encrypt(resultStr, AesEncryptUtil.KEY,AesEncryptUtil.IV);
        return Result.success(encrypt);
    }

    /**
     * 查询所有的图片信息
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping("/getSpPictureTwice")
    public ApiResponse getSpPictureTwice(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                         @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                         @RequestParam(value = "startTime",required = true) String startTime,
                                         @RequestParam(value = "endTime",required = true) String endTime,
                                         @RequestParam(value = "lineCode",required = false) String lineCode,
                                         @RequestParam(value = "isDanger",required = false) Boolean isDanger){
        LogUtils.info("开始根据条件查询每天的过包数");
        PageHelper.startPage(pageNum, pageSize);
        List res = contrabandListService.getSpPictureTwice(uniqueCode,startTime,endTime,lineCode,isDanger);
        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(res);
        String resultStr= JSON.toJSONString(mapPageInfo);
        String encrypt = AesEncryptUtil.encrypt(resultStr, AesEncryptUtil.KEY,AesEncryptUtil.IV);
        return Result.success(encrypt);
    }

    public static void main(String[] args) {
        String s = AesEncryptUtil.desEncrypt("2hBIQyQVkek9qhTWyXMixQTccbCCfb/9GxPxJlB4rNNwYZXgq8625t88TcpySB72D9m3v663QJgZTTXDbURiJKJ8m6tj+0Ee/QaYJVzRCodOeaRldSEZ5AOj9i7bvgpYEOxaFE8Nyfr5fcdtvyhVZI8rqD2F4guT9d93bIMEVxObln8a01PorUsGIF2M7A2WtoKZBeJy8XXltMhR/PBnKL7kKO+bC6zv75bmXH5HCH7QMOolLxvsIwu1aHwY6bGiDetPTWKHQxxZJ62RnMQKVO2WUrYzL13BjDSzCoMPNkKhewX3qUxwWHq6w6erQKCKpiT1tdSaBG18An14gZlUSrC87kNDSsCc0Z/jnr4tBle62MKSbSNx7Ot1G3fw8yEaocrkLeOTkHq9EZlQ6VNvTrXW5JlvtHYIuoc/lrd7NpcbKBnRv9r/1vRT0kZZEyWhdSE9AgF/ck24s2NVNP/Gx3TNnFOxYnL4J/pfht2EjWCjoyKwLxPVYTYxMvBZC8X3CxWR2QJuhxwMdV2Zh9qXQ29U9L9vnmo0vsH3m5okci9t07aIB4eY4bPpV0jhf1Sr600sJMswDm655/7jFUgJBr+Kiw4lXpimK1wvDb8hBmoB6p3dDACWF3CSJnjqv07CuV9dV/nQOo7VDNOocHKigFyDfPJJb0fsCoORu2q3Q0khoLpzQOj8UrYs7cbDBTRDE2WohkcM/XnJu2j6w29OPSmkWMylgQ7cAZvbHvJ9MmJVV7GrISuQlDi3ax59L5Re04RbnIkkKJc4Q0WqFomyF6Eb2M6v62AsxxNI730GK6lEk5rtwYR2tkrQ45FxZbhYkGguBQ3G1VD2zTOhF8k3wDh7KQ7gNa93SSmedK8oCxWjEUuG2/dhLoaCZDm/tHUm+p3hlI+4kT+M75ovcIbRMIKu6Jb02BYupzC8ggP+TwoqtBnEQefUMist/xlZ0jHzbDKjuXpZxAr8UK5/ste1FCHtikuPCozhS/NZc8Qjtj1Gp8MGc/nKmrU5eCuGHYPI8SUoA+WdxAEp55i5ev4Yhf08+l7idd3kXURC9iFjrgXuFZXDjJ4hwnof0vRj2Qc7eRF6/vxLpmPnEhHx7jrW0pe5uLrraasQNM1LyH9Aeru2tJFGg6UJybbXLbnurJsZqIQTklPOaCHmfhNhLnIMNCavM/RCSYSnQ6tZFOKLNAglKW7a1bbQfUl1cq7jzNYURXESVEAckJfV7CZgW/iKaVRsXvjWXu72OxU9SST4Yi2kmWyPmm0HCVi0RoAD441R7XV78/1IQecgf09vViI5NDkmf046zPPj3owmZljVxRK2tjyihbFZFj5/d920nBMD1xlnJBM8fuZdqFTtKzT4Idu7X30meKuBdrY8/Ijdgyrq2A1PTA3EM+sGAcjGmGJEeDG2q0g8efjUQ+Y0aIO/cfvDhg9it532WrlErqM3DkbyPJ7lP58mcul6iblB2KqyOvFBxPl9Jq08TLjD6rY7QRuFZ2r6gyWIb0ZefVJUesqcK46kJGxuGmP5wmL1/dLy5F8fsyzVYH9mD+F2Vl3c/NsQT1Qy3AIJ9kP5B1JTccijh384Y9yc8/4rEVl+Dp7EgzUIIVMRApyg5Lu7vUrSincyhu1Rud9bXOJ88wOCTra3tLs72i30w/t4WkltIMUxpnYoPUchGJUpFBRH6EyU9EXbgwQ4VTN3Jvj1LsZvXwO9VbUK3HNw0nCsuBCFMZBaXLiwvFgxmrDNDwghbS4OK0gyoo5ReRmiBnroSyBqdCGHeizL+rtKDKB7p9eaefIrImtS5Q1Sh882Mf7bdTBVacTExhuB+3rAOv7OgbYy1mUZmSzV0WVCfoJ6yON7tdOOjvClGUm5HOlpRD8nAeDDtTPSumpVT0Ir3dRKDD2RepnP6VEmTH40eVynOvoGSUkodspmS+7yITB4U5mZN63xecVCpdX+nEnRtZrYS6OzRJgr+HOk0ga+ppX2UUlWuHkzR+2PP9ws1Zle/oE9iThj94KJRLvlXmMjjjOx4DDOn5jDJ7NM2djUdnZGN2kvn95hfaYj0Lt5Ta8cR6nI/z6TeF6t0WahNijw26r1a49x/rLwxDtGf0RIG/id4M8pPwAmQUz1BIWY4C3BqGQ51/Kho+W7RZcIyjc5b2v8sKo/uBWoX/njMuEKBMYtrkXQ2jXD5cskJe/VKs29rGmHp4+YImQgdvs7FRp7d/fixOlvWbInNw01wQbozWfOIb9ixqtuFKqgu81JvXFfP5SnlD4pl9w3BAeHMea7C45f6rO9d/jBoC9r8YG6S9Tbn7XitqINWQhPfoi+cXmCELqXqzavSwDZUzESUVHDOJjOzObS5q3RWB1ryzX662Xdg4MpRQLUNbKhnw5WUZaUnam7uArK6QjpViDrIkiyxNMPiV1tDsyJLm/0ELosGBG97jvMt9pAeZA0S8fG2JwgHcK9VYpb1t9dKzeO4E9crfge6yy50j3kQwAutkYQ8wNqonx/d0Zs/Tva9W0w2KJH+/MjRrbtygOWrYsEG+GfOXOUAKYn02WQVVHw644dN8Vqq4gsc+jdxULNN7OE1oKhvvNh44QeA0nopauIgzI3w2N9ysM1a8KfetF78/ZUtaWTD7T6yh2r1ISBPKUNqKdYh29scVbDDiQlDiaYZbIheuUF4myIxCpvcXirHgrDJFTq1PZhOZwnX8sw/nNlXWEwrhVH2XLKSt2ScvVyE675OxlHOMXYrQdc6QaSgZmI/AhaRtGIrHug/y61YUJw4TAYy5970bBzz1YGBsVpgraGX97n7ZxXR/VICPFzy4nNraIPLHfb411Htpb2m3HLqyX3m2TBfuPbaolYZA7rq2s5PRTremAWoLYZQiEA75tMW+R/CT9NeztnqrTSp1b95pImmTB+x7KjRvVEy0NKYTcvYUNKNbjb5UzlQJYfCiw1TcfaksY28+9WLmxuwMr3MLk7JKvh0e/ylByYtq8T2dOi2E4XgNz/jsKaaO4Q3ha70ZaEVJvebtJJpECY5iOGffWe2+3L9HdYam4JO28TxecuMtff3SjiMSPUxrZlF2Y+Xr4lVho5FGjTyOgV77CaIAqHGDS7jgFdviTxOfSFb+FS6Vcr4j1dJYfMJYne/UpRKYD83+StvU7cwLNb7sAw1/zdRl5lqEnf/N9o/y/fcnllYDKT7/DZqEqfp25/ZuEnYgjZy9GijIQrOxTnEsSF38hQw+nOCft9iCUDEqSGgc1eQRDKpDlPm7QdfpIB6A7qiJhpguTN3suOOg6GInP3T9BXmW5PmwohDuO9W2knxxT5k1/BlGzpPIN3YUBnUme4+DWSG9XrRHWunTf0A7dGUhTnhFC/HPJ20KKbRzWVr0OkXGS+bSZqhibpWjuCRV0xczTulNX1HBOUIe627GE/eB8uEsHvzXGfM3aQsqXvyaKzE2K/cZbKX4fMYHZi4mGvAchT7hB27kSQovFwTo7ZuwAby/Atqc0J0kWxSdG8GMLKOgz8JpwYUh4kt4xmGhwU6sGL580fG8QnJmqJsujh9vn+hPpOHzEP4of1RralQftvxgz4dPxErwSJ5uj8kJiJj/PRqP5lel09QHLSzVlWcj8CfEXl9z6tRLyWJsKYLpPWA+avzDZy/Ci4pJqmzri1foslezkmzh2e+mh9faeoAbCsMqEEGQ3aCp9dSo+lJGWy+K6y8kMZjfBF8J7Q+NeS5lIz1jrLDG1W/8tsHtnDgl95+x1JnUSDog9rPhSBjd7frKpPLYNcD1fdzeDBwUpg6o4nIXMDG21cs+Sw0Eusgync5zPSCRZAQSfJKqGxPezB4otoFkJ7OqfB/hdiWWlbEaH+s6wHu9h3OagasCpdx2vDo9E2ylgojOBTrKS2luSqpnqx6Co0wKAEBbIhJCi156EF7n5gfgmg6vLmWA4svDRyDZ5U4+9/K6MlTWav1YPKAXJsweA1DCh7k4Zk1kq/4ltoBc2Vt6n3g5bovD5YAKcOJxG9Se8DUCGOntb63EBWuhCuSI+r0GeI6lhAowy10tJTB1HdWTLjRhbG+FLgFcvhki5pp12Y9cffHQprbbyAzW1qE5bB01lQ4mENwuknEhQMkh3kto8WAU24/yAHo/3iGkQ56X+DohtjQIL2p88h9WZ85aFhqZ9/n0/ntK6+eyMIbflpAIbzzBSRQUH0CpPmJLRoBD4hP0JF8jlNp/RHWSv1ftXlBMOWRuH9+z0DB/uFQDtB9agnNMdx9B1K8tvkFIB9azFdJTiexriYPH0hdfrdVH5iMokQRPqpPgmOpRMD+cIW56OOKMcz0kc3FBuDxBizVg0sbGB5D6yfUAdJlqjuIjxU60sohwsig04vSiuphN/jhJq0yGb0s/cGrjcdH2yZqqHfv4y8KeMZrpzWoLidQQG5L/Zl+lN4VM24iVZHFdRpYV4WLMxygNonwZysULvdIcSZlYOhsrXGEQlSGlftEMMx+ESe6aOg1fbmZW+ELxHM16Vjh2IOa+4PAFeJM3tJYxikuUutxr+NqzYmJsu+Wjit3HYD04OhRg/OPiabvNlJH2BJI8tpZT2MLqg+fW8OhS60GDMtb8XIetD5UBfqkLgVw1eJjGDWyBE/B2aked8s7W5coy+QpkEBJy6p8QTjTVJNns+/AKskH63fTs3D2tgcjG9axvUzJeBAiTrNB5WokMXxlJwkMOQHHbqBkdUqyNuDPWh/kiYnbSwmn0OGj3kHbz62vOWBa315yPA+jMzJJbBuJqjsEa2j5230wizyzh0kHWVI5Rblp7oubIMwxMUuVmcquOj2OeLw2a/czsU4mfb/+UATmiygoh8gqtzsjDMDD9Z+8wOsuxGeKkyWtk8/kaUh+rTCtKVkrcszgW3kiuXvywGzHX+05F37Y7WA3YHpbbZjmx8MDp5QbeVHJW99B9C2PSA46E59cKqhgJwHQBiwi5r12ylZHYY7idgl9wFwLTM7opXRKEb+H2w6iPQbmYreYdriEURvKdDTDgLS/DyUheR2OAjsMVLk9vG3dZz4Ykdu8V6mABgd3+aSZHqBEP3U/cGLl7ZPy9o0TWu+P/NhrlFTFJdDYY7yiNpWrSVhMrUmsKtowXAA4hTYEMqt7VVyVNK2ChBI9UH/L87GGHZUkfTl5az5EONkaXqS4BymeoJgvmXEeE97Ig5XeoVnzgpZC6lF94AY8enS74aGkWIi217Fn97mDtq2YcK7sGEbztrybJh3TFl/yhjrUEP6MZzkXnE/hBXjdmCuS4xf4jvHtqoJk3CLZleK4sCR9xYEPG2m2FkTbi793ctqQ8mn2eRgobQPK3kVXSSDNXGIQldSWZbj6WNJpeFXFe+jfQvp7QOlWk73JNJHvU1pzzN9+tFbboAA/AeuE+s8AZrsDVRG5iznWVa12HbDvfzn+6bn5A6RGVwgtVd+KR9epc6oDkwVCkCPxio/S05ra9mGPrcRvfauhfdH/9aqfxc+htCclXb6lyXxMBsLR74JhKNhr/cSGNvJ8AVLDyA3m/TedBhrHufB52i8Duhaqn7RPPnNDW0S3d3duTn6qa26kodTUwjcTM8NsvEmxwDSaEY0PIUo07EGDGxVEjTotGJvNvW0LrkK0DwOU2hKBfEa8eDCo8URQChnXMfaTkoyXXaFLR/pNsqLGwBLc15Br/nkPuyUaG5jNLtLW9jiTLZxXiXBUSSSy8RAIrfEAz9Y6D7XQHHS2IgA4WyqMN7pDK6aj10JNYP/DfHSyYofXUCYBV6BEiN3Ut4x33Dzk2tombDuLMGjIqMQYI/LJHYtNs8rAODERCvtbjA+mdUHY4B5IeBNuj1LKtNQmDUy5vcarmwXrb+KjS4Tsv2Awicq2xne6bQG4IWFfQimVanpKTrS45NME5L/Z9bYXEwTt8x7JSAYMKuYQOv4YZgOtnegwigVkLdWRzsMJTYbgLiJpPTItjfEMqr3nQEz3reXM6U8r5bW+ldaoMascqda1pyW/k+H/o1PX3+qU3epcw5MUPSZGnFdCup07s5WXd6T22kzsYxGeXwoUY0XhP1usUTL4+YgE2z+V0tiNMccvbNy7Y//GKXHgnMnc52pDs80Iky1kWGYBMvGCobjqN6rwJIUywPWdF9SraVwbSEtQr0pxUjPRwXiirFGYVjq3gAaZ459l5wfc8n7ZNFCaO5m/iKFWRsEQB3D4FJAXfa+l86rXq8SCxyL23yB47ZDRzYC2Ktw5pwJmm3NeNTtf2CuzX8ZNRsCIZjdUgKZpALGXgynofoXh0+reHrAwaL80jMLvCCWeiU03SM//wT5L5VxATuMT55J3ZZka5A0Y8ZgctxLoSp0v8pzWco9sbtbys90MBjcKhlaUVaf3phiJq4EMEnjw64bonms97qmezW4TCgu2MnspwWuvHwS23wZ6cZTgUsNCGJxwpb0BPpJtxsghNwDRp0WmOi69vOPKY7WDMb6PUCdwyaFLWQ8AT6sV5tXU3cjsFJ2aELLs9BgkcDsJkwPOhgrjEVT1ELzYWT3IXkwbvRlJJ8g5DYlpdt0nfbjODEqSeDXsgKbgAsOxqZbD7JVRy17sjW4jFd/s00Yy+XOO6HAHFUflLPpbiLey7xifEPNhrOFOAAlAWfDb5nPbNHhgAVWpoAAHn55X63fgXZtpGhhcVc1qUqzhVv7RJv2AvcN4d5iiqx2orosl2N2WESepVZiysULoK8IdtejRuZn9rgGiw7ESwJ3SaE/nCnPSmnRVuHMNJQ4KQDvrT2kFml85P4ZB7/Y32ioEbPx++btJ8EjR6GjemSDB1J4+4aJNA70JnbFZ3VRUJmz/W+1ZfSQKi/llt9HW2Tp04LGwfqubthhuEvJAGF5qaHzjrcWaDyQQJ6FA4xbIviSRiuOUZ+YI6WGWUDZ/jkfLNh2G4YxwmlBHi5VAFGjJwBDv3E5KiGiEf0Bn7Qzr5oppZ4YUuRgIOr/EzeI7Rgw7/RWjDNnwq6tidx8dq7gw6Tr8qlxisPcLdUCIFfDfVjqWL8VBy8JYJu5T/BshOBDnnReVSXJRerOjhoLekQTSUgvMOAxgNz9VWXTQL2PBpbEVo4SyKFUVH2X79hIaqlb9yMy0l5CfW3ul6ACb+1cBVURwtElXfRPiaFezCXkus3RBdnztud4mDv4xMCCFQxEivA/CUczsocJwvIXnyM+fwKXYgm1DM4PxZoxuKdHQqI7puGd36LQ5C6pQh3tWLBUk89oclTFKA80SNTQhildxb+lvJ3HMGJejKwIbROxyMytOo6uJi+g/Yz4n6+tKsrZNF0x1NicieWlkx3PaJQUAutnxgkutKRlk0ViMR7lKX7zyLcW7XNTABCOxedGSLdps74SgvYD2aKAu2oO71DwVg0qnlMjxnNwlt6jbQd/i4Fg9iYY4y3LgLK5D+8eLyqvKP4XIgUhgLS29pV3kDvnEiAyp7oqiid+BDYPulODn7MTGkUrscMSDVBqo0svPtGUnZ5B1pl0kdyQ94fWKSPpmgZwHaTiAgx0quAnd3n+FD0lLYOsgn45EmlEKLiSokkSC7MEvtosbVX48b4T8uBVIOlz2zj32taW+R+qen6foOK2QLZiDpJK+WlCN/XGhYNCJpuamT98jpf+fQyCrAIW+EhOPhJRG1ZYWhsU2VH+Y6dTjjNGRL6kPA3TZW8SsQ4oCcWt/3RUkL+C6qTUrjdQbCQD5/Rm1FlZNTBAlF1QxrUnIAx6Zs6zRTQ7YxGjBEahHTypkU1gFfalLpqBzFIhNzGjVcohRRVMI7mYlfbcpMTOUMhYy7D1GDjIFqQuHOdTAR6hPfRJ817Zc1I4SoYdnluAFdxEt3dxLJ7Si0UOryYmrRYEzH62X7MqMQCVP5AYwL3DeHgq/t8M75EqG1y7/SBgnDdeYJOOT609owScgIePZatGB1g3sLcTfNMX3eIsLrMlu7i3dBSbWDdEJuHECqWJzpbyfHv8T9vFdimKvZWM/iPPAH6EzoJ42fq2Qr6P8kf6BWBaRH3n0tJUZFJgTbZcamyv6+HG4WbP3wWy2j95dVCqPhejVVdZG/5gbpWrv8216G0PJp8Ip/XXKpsuTgth8vhhFpPyDnh7fg4QcmomORvL6PWf6RlWTorml1PPAhOf0FF7J9rTr3YpQURWaKQihHS9dW4zTuhP1uOYlb7XK5BDuFri/TeAmt5+ws0LTAOhw9aIVUXwKEx+6QaZC3JDeWHuRnWL1ssWPfEdmmR6D7FAwLjqy/jNnAtVyNgBXcJpkFaOHQSoJlrucs5NMHFDuDi33sQEBNG4KdBD6tHNya74JfSaSD4Pw2uO9GLh6R/nXHLDch1d3F4WDNJC4ZncJSN7F+AqmZHT3nML9dUF79Ni7e3GgjVLfWtBxbzSz7CcuJb/5T1O6ENKWzKmPoxGV+X7/+P3DuftUgyJkPqFR7vV2eOTEQe+HT/WvkTWTDRbKgSIHEbOHu8yQ/t39aTEqkNZMp31FNMeDvOMHs+hqlcyj4Vl6MU4GXCbuxDTBfxmIcAd4CeQKQDgQsLiYbGv1MBjOqoyI0u/Fq7LYcZU40SFcovW2T6JXuntEnuUefEGcsxwwEAWB7/Uga9lKxVaXsGYQbusK136QPExIVZYk2zyga/G/NyM9rYRfVzUdD/mV7lXc87MYM7YurZMiSC7WW33GjOG6AYWNN2KeQbcU3pd0eSp9Csg0z+qn/1ENt5r0VS4JxhfFinBUXhO4VnUcOgtdMHkEWeKPmB1S09mtSsRhuQqX7pmBiktA+hlfd3YMwU86DYku+Ibl14vRYpXcMY+lBQ+aPRDPonrNeKd7ife4XtFlRxBGh7Xv6Zsi/ARaGWN82eh7bKuVWqlI0IBhocJEuAb6iHGTGzAitWs7OmxWp5k536I49Aevkr8lDPWePW24dvXlCRXCxMQKQ2lVNamYnI6PrCRdKvTmCfHZ72GQ3ZY8Dq7IQ1sqggqttYGxyu3MPMcBS8xbuNjd6FXXLpEgkMdIN7NH4fDh+lO8z2Gf+8cc/E1TjaHv58z9dMd4uchfDnJ7juUlEAjVFb/E8aQvSd+J5RPB5DSQCY/QAvElzoq/fxASVKDLSK5WOO9PVZnNke0zMyFLkJYxaqK9U6I9CSVY09KBcDHxBbmTdLmWbrJFCXPysTgL8mp5UUcYC4/zFMr5Hu10p9ae0ffzSd/r/MBLX9eN3EPNz438Hr4xvdYq75Tvx6LCSFWdXOiapnJ3mrZLm/TUnDSVYd9AIbejPK4Oz56XMxSTD1KmveCchep2Uzt4SjkrOPGgqGEVlrDO1p09bBkmqskTynZa6OPBFLldeJnrUYKOHS5cLkzIWh1USG87RGjz5Zx8F3orFzeaQaJvrrQHrIDkbRhEm37H7dfwo0xQ/aOsZFVCakqHv9MMCd4zyq2RpwKgRYJ5vvviPlK0sxrDXxo2hbcn4MYgeryxEdd4ps1IGuV3Jf2mfm/Yfe7WIwL+P1wCAkXWzwcf+DTiIRcKzZSSiWKM6sfpnFjGZLf9+F5n629TYSt74VH+RbPBXv/UvcjhHYIAxeeJpejGPQstlm4AxPhBWCpXKDV8RNjuFYVVgdqsGOaOkT/bzbfR2PFyRfl0PJUoZT4jiM9vd69qH15nZrq0eaz5YaLJpFZYUby1MOPDbXtg1OkCJFLDwYKam1AxI1EGvqPkpwrJWylYpMqeLUivmST/kyxKLW7A45sDWdHCE/kEJbNGOoZC05yqPQv/+c4i8HFgFW/vQjo/GUHqgOp2Iux8igeObmkV1F8lGC2yAavB9jswL49QvG+nYUmpY3Tv9aWmv1FZmuhq/2dlwBER1rDDIJZ6gT04uZI7nr827SfcFaW9PcE/o7d5w7OpyVabtL/lfb6HKaPhuKGpLuyeB/QN5Ny7Vxbp7ZpA6uuShjrwyZeFV9Y8h3d7X3vtiugKqbmxLGfE3omEKNGms+FWVmHxATmWGb/VD2TkFWBr5Xug2SdMM0GpdhnBbOjKF63zOaXRHmNqMpONkvza56TP0b6RLGbyPDq3o8cY6JaRR2ZIvipQiWM+VwMxP6HAiv9p/bCs2YN1oHR2FpimqzDM4KQLPEo20/+HZiziAAwlbQUmkV4ODkpRwPnIvb4ILBtBdOOvYrGkd/RVtaEqUeEdEwCsCe8pelHyVrThv6lBYAL5AJ+Goic3SIWDbxwYN1v5mzNkOoE7zYbCX5uwkd5qfLFEw4zQMMX860Nj6vtGHliHy1Dm/cee2FJJdQ7v/gZ51kTyg8dxIA8nPdJKH6Ejv9tXlv3COtr5L4YIgL+xpWYaBNa636W7LH3VaOWZ43wPKyWa5eVTrEr8Ez5TGAQBe7mAk2/s/EcZjexJfVVv/CFmvnoPOzre3tE0VmgFFCrShgTcOm+WlGeU9yDoNXod+x07JIgu4MQdcEKm6OPTgrf+FTHR8AaaTQWxVJy0cvB0bOFWrjwqutguo93R+T4IqfdC/pTJbrLGYj7Q0L3W+BkYg9WS04NAGnRO1Spe3O5UBEmZ8sBqQlQ32JmXjElSG/UGDkfSi/99TB+PW2pH6GWdAsZ8Ug2SN21SxpQQl5Lxuuo7ByFhJpv5ivacCVtnqJQslNxuiianMGEvjhQ+cUNnd/XsCh7WdMvtQ1GCHX6cOaIlTI2RDMp69wriBklGT3E8bQUom6Fu2YMZXem38OsQnXEHjczb5o+9r6ss+mXtgs+eqmtvRlco6+I1ulc/bv0SIENPJKov5TYcymedvta9EtIEBKlqsGiFOYsU/pYws6HS7FI2WoAMV3vd16zJaOdBTOKch6q9vPRXpKb4jYvIgEPTpG2fzu/wnmMZGdpQpessA7gvkPtM1cRpocAtfB/WtOF3H7nuedOokFk3cQ1rYwjkz905uFkr5bZ+O2GQUQh8D22vXZdLc+sAfLmnAf/wtgdPfpGqc7OKtyTP8Gv2u7sEdDD8hQViyn5hPkCTE423D1535LyPSXVW0llnS3METnCaYyPNC2ru2qc21h7RFtXvKHxdznRwy3at/fcd1+Hrl/FkmhZD2Pp3FljvTOOB24jhRDCY7ZaThOi6/IVgEyXxdkwNRH1pygF3GyUzG+O++XtTkOamZPxBtKWwdMeGVQZym0MTAgsnZYjI80agbhRkc9URRKmABKTlffm1PKmTSy8Xaq1gLU8FKdxdlbcQkLHvJCeWrnbT9x6nYT0J+XQCFqbQ3SMKm5f8nqGRsKMEOXJH+CtXbkA7P9+SStITIweyzM15/rZRpLQvj8CCgRk/hLcO+75ZKMX5f73aVDnwSo5nhZQkbrE79/7Ht2sloWL1jYMsEIb2/h0Fnj09xlTaM+PsR8ew3mNqkpjnTi3d8+gZqpEBXRnzp4DsOn1vRrSrngy+IY4FzWkellAZQOBvaeUaiUfi+ked4L6QbJAokKZ6NeK4ZzzJSYLHVGCIXFLv63fn/xspCulNK6+0wV/OYjTFXUz1QWoxLfkucUWKxohQHWTAeI1XF6FzyewGi4d184EU6fJxlIpimrRvUhkWg8aEQaYop6PDVcmS2QRzOIeKVApgJIFLhmhbIrVGUPHYLNPVbPOQAftvpWkY/jquTRPD1QaxIg+fUlatBRdyQM6IhUlW9r0Hp8ZOj1YYmpvt8tIZA73CUzS3ybtosl1TjiYER9FHqDQpgIi3Dg4X9pXpd6WkZFMv4nWnY2mqt7uDzXCLh3x9FVibREbVOXe8IijhbLRHVBjX41nwYNjNVPOJU9aodW9GQT1hxAIyk7RZ0tZrF3s9vkwVRL/4wTyd+f5oLFRWys3/wFVDCryoOd1iIpCrYQ4/nQNZQnnXN4re9ihbdWiyqDEtqyV+/GtOHAmpkUHBdYmDvNRcMqKZZHtxVChBFYC1W/DlhKeI7T00efFyUkqbBTwcuPm+YpEB5/pOQjMAbMJr8h+zKRSntpJiHw9ushqhrAVbguUbGedBmnKvMPN0KdtC++Vwnz3QZ9CIXUUDNVFjEBUrjZKhtIZl0Hf/zu8XKCpAcqRn2+aPhEOxPpSlkH1+ves/a5aKec/tvueQtsZ/Cm1Kn7GfwlRr0xz9Q+bwjllrVX8MFBBTilu1O4bZow3TAj2OWhvLh5i1uRVDjowkwLtC/sHnMTVOsVpi/rksLudWQWlHygLk3mQAqz+jhsMnJeUD4FRcbkfO+vzVsoLNEbPZ4yyKKofYGRCCbFbprS23SmS0aaa3mJQ+mu+TSrO3DkziC591YtZgnyEosD5eS1JHD72GE8J55eeK48CuqIvOT8sWJuj+9gfBBXQIfx/meGSXRMvPrqOUs/h7Tkh/kNk1PfyZqngccjPr5nW53hqHml4YUlOQAQ4PRYDa6NggClSj3PB0olkb71vZVhQz3Gq01S9FU/ROy2sv4E+mI1jhEjtwt56HGXcb92DpPkDuIgvxW6jpbFnf1r3bdgHelgNMWd/rMBPY0VzlPficSBGEuP3oCOevGljNHrj+RiSVr2BKFEcZvVYi0+ULcASnqUCkj1MnfulU2EqfkduaD4vbhBFkOOURIteriQi0ZTJgwihKd8oLRqL6t9Iz99rVIg0pkppJQ1w5vhwPnPwnx18VVL20AvMR9p2intBQ5XaDzRVOGuJ+CUV0gu+IGn/KViOVcuYiXYaGZ4OKqiEORgA21iE39vWQUtL3nN1u+O4g7a5Clavu2dGQVm4pAA1b/NBWwqD+UC9OIG2HSzTKzf5er8k/no2Ri72a4BkW2SMxWvHa5c3Knz2b0L34BVxsTz1glMWQo6eF5oOsrxPKNJlBMHqG36hbJpbr63dSHUe8S4sxvAxcyrskmIJ7c45Lfvo/GYDgvWeXBTmhJ2INseFwkJwh3Cy1TcBZr/h7+J2HfojCRnYzL/k3ywEbfCnboEyTDmPI+NRO94HFJNFRHfALWYgMX3r7qKzP+GmAztBL/LI6O18fv5UgPcsSZ+KoLvVVI994ijGikQ138LRFgXNzL0ah2Id+y2NOPA1GfMuYSP74O6NE4sfrcuZQGv4TYpmuT9WPExLj33NvELvDc84ER+kPbdwrjSDLDHrMzr9yR8mH8L84B4LYrhOjQlhX+LDILD656KOwwNMgJgMADomFe65Nd4RBQYU0CJhxLBiAIjUtyGQbm5GB8ZraD3IWfV8uqGx+jXRdz7M3w0b6SrZmHME6pT4Uq+ls1QrpPaWatvSq8lKlE3GtdbMw34Cip3/4U27krNWgGAj8KS9RE5y7thcov+ybQjkGYzfC0eq6vlwS9oYNAkwdTGfpV2kx/YOS8YdczgbnO7RbNCUwAdmplgPkbfaJPb3o99eVD5SaeuOTjlldyiNPzYq/AEWtW7CBm2CYLoqeMWQkqAyQK8K4McP5J2hFmGY2F28h5PG2y14JeUyqMigcyn4rHsm6ZReDe4nlT/1Nq4hpHaKmdBykAfErZ8qIkkHJxCDFJQCbH5Fe7+V8IolL9+HVqOmoeDxb1hts+JOYQw8Ub/31c4vC6apNH9mituYZ82C8aVeCjc3JULHc8xaCPsqBwiynYJdJgbNGLSq+mABcKDzWHWviZOzDQpjBalwGtuVzZaSj4AWZ3Wpa9hqjPRtGLiiowI6hMugbdvc9M5acEV0hhGGOjgXB2E4IodVP4py3M/Oj2h0G9KNjvHXi9rqteKKT+H3Xo3WW8BdxDh7zp27whL34Rku6Z86KUsi9+5rlvgOgRbZudVpNllY/zR3vTstuiGXxdlUjWfAJfvllKgNwFFnPIqBwAsgeGQFgO84dOSVucLuvBNJtn0kArLJimoyGhnrrLqxaqGd96h4yHGzgjdwfQvWi4M8wrh11U1MvgHgrv/gCOpxlIbS+2D3ts6vf1EZn3CJ1rAkMJDw1RtIS9bAB3ycBQZ/lCEZvMNK4vLCEQfwkngae/B3iU1ybTfdQqbwGu6S3TATVij2tJDzjKwhB69F28xncYoMnbbo1JGF4gY7A2Qgt+QXrLU+ZmJkrQey2/YhdUbwbpg0NejydBtk2UptzBKfX9frr2bN6zhBphg6pcRU8QZ9wJm4NTO5HJl6i7YOrPmpu1Vh70UYTCKmF+7c6pdkyHQNIjMP1aNQC7bvo3L42JZVwccq2sOG/I56p3ncNbIaVydH8X5YUt9I4JXFGnj7WQtydKqL3RX2O09O7vzWMCsPC5+GFvDh21xe/19IqBuDl+84l4X+V8ki6kpJHZim7U0D9PHPiuhNOyfw7+YZg55eufpsxgiJCfr+RfQc8lgFNQtpa/Fyt1I7+KSO51EZJt+r7zxETE13KudLTXrL5QInHvCy28xe80ZBDLeIyF3PE1plrBYrQor0scNDNTDY7eXYc7GpgyOy4ejawI6Fp+5O3DUPOlsJ80rKbNhGAouj0AGTCY8iY3CpduFf+kTHiHKrhV92Ten63rsooDzsMLZzoiEpfxJaJwE5ixdkCpAlq8uwu6P+sVKxRPHA4frIbSA9OunCg/3lf/rTHcVyuSU/V1GS+8tQxxY5Irn55kYaZRAe0c8kHCYEr/KbqxeXjKNsO49hAsk3LBT22bZ2w2RCjvKM14kCLqfAa4+neZ2zu9ihv9zgCK8pQFALCbCVfEzQ4To+whX8HeeoaDJv4vSQGht6C6365D7CCbzmY1eaD7L4KDkgnVJbEcqemmxg/zARJ2KLSQhWgPjrRruEpFAqSDQv5begC1rS1y5aXliESNhXxSEF3lAO0CvIkJqS+R0yrWnHGa02k6tNduRK4gdbyiQ3Y9ykSb3SzzcCcLAC0Fp9xW+sVMkuHgKGjpRuKpH4HwSo3+KLl8A2Xw3CFs/3wKouFYOPaoE6zb3yJgH26cmRuX6PXEq9blOOP7ill3W2QKfg2x1BRG8Win4bXM0kkVbGhsY89LskiNrma+H4z0GlrTo9JFZHqctquS0H55w4xEb+/p6/3nyYId9+BtAvaCEsAKwe8xAKOe1ocbUEUVZvE6J/yx5MN6V1zZ602tSRFH7Y//ev1nKJwku7szHurP6SZgn562g/P902vXbQWYkYRbQ3VE/2XSGDbHoJOYtH3CsxoUHJA8mF/9nUUQVBoDotxwaOXW9sfQ4I20baqMNpMS8YHJMAU4tmjeNFUWM394dqpqo3NxOX8GIL8yEc7BjX2p3l9INECId5nV0tXGLiR6CHvEtPcmNmUEMJHUQMJbo7RoChG43u2tU6hNJyGtfnExuAhNjvL7v6TxVH1yuKs3Q0APXACnOwJCZo2lslddy4cXlcP68zsiaRaxkZFGxE/7nDfFPpALvVYfl4tbGyK0eEwVuK0zBdCstVM3xQuNgnGo2Y+DvkPwwuO+hA/QQavDTSYFApccwHttqBADr6poftq44tQa/oG77sHgcWhY4e4mxiWl41qYgnTfZIEHRbfFyV3mPCtbT0r0tuAHa5eYqnlQe4rnLOmkhUzOFhT4Blurb7J7XeK21jZBGRg5HBluvj/U8cQ1PBunMCme59nyZs805rokeRs8EcuFHlx7JM0eRdZ9s8y99d3g+9CFkZH5D/wdlTHKFsZczNYPfv8SB5Z7uEoU/sEnkmZWJLocoi3W9Mm6uoxk6fqm+G7tbTXBmZhXUAT+gFVf4jISzQNp9C5muQtqAYUSVhACbS/9DjV0wQJfTJfcQ3cQSwxnMzFoi4O8ohxbNhtNah0OXta72hF49SCqVCNRbLnkgvzJFXnRCLRpkN5AcUnaSVI5PqAoiqdncQvH1GIEx/HIfhuoM5lgJ003RwrFlcXzjegbd+EkXDoNw+5soVqOEvcKIVeYtsZfGMOkRupgG1MmB1lx5Mkfkgh6rghhwLFcj63/ZN3pcbatb62tpLIUsBDDdrzG/s4KecadF7t+XkvTsI2uwluzQtUZddFYiCbGLmUqKPr+VxQZOR5H1XoH5gDpHm395PWPBYlqFQThsOTA4KpBYDQczHGJbrbDeC8x5bAHeInw5Pqk51+rQI1U2ulQuSOspQZJQrsVPsncCtTpt4R053oX4azbOk0sk7uex4Jx0sykfRR8B6KLEspt/JLDyeBGq7OpmWzddrxTQTRKKrx0uw4H0IUsiKKfmMtUxp0BV6/yrYzEhcw81Uai3NHeG2VHPQOGJG43SMvFaCtpAnePyqn5IYYIFBWTpX6GGp+kSc2K/V+Is30RrhR1tQG3OsEtGxOZZrnaFQEf6wJazGIbFZC+LzqNwA1hgq6/9eErbAYvZVPu/47/oWIpAERMFxDYajLm4ULDmc2IULl7JwDZAvnPVm24Pzj1n2vos7VoNnHF/FZquYSVWNAY0OHsJEdClk5AkT7cZvAH6YuaxzeZb7Z6JJemEnv7EJ4dXfxGeyL0vaAXwa3wvrkEG11Pi/odkrBIIqMAz58aJXQUkR7dM0DVRxqQXBIh68L/MYEgIzAr/QTSFzGLrFzWecQjtWLzc9VQ3A2EuoJvr3LR4ygDiXeCvIXJcpe09Znh9bMeYJDAa/0P8UlJJzunkvUMJmnganXxB9dtkqIvaAL1sEo9bmcspYwTzxAUiDtXwmoeSNOCijzQ++vJcGrdW+h4LPyMjYIoXOChxKT/65Ic28d2lPsckun1isROlIJpb8WBnKsD8yMryITpJOYbofbH64uJKuuUY9WOWzaXMDXFc6vYCOrzUvCsO/KChtOAP4oKA0NCesMFqQJyBAL5Juf0Zl5FDkA4m9PG+haZOqcNnaYs5x0Z8weGiD7r3KjAne+nxQuGBIbT9O6a7OpnyYh/gFu51jG3BfbhuPscrMgCMSJBm+8n7M/2IpadocCsT9APm6E7ifiMZQNOq/cHaPjOMh1CA+PHTmRnjmAOlJAZTPi5qbyhYb0kI7cpOP6adYR8tvnFNiSs+jAcFdYhKo7x5JOES4PgvtLTE0JfA0Ge+PO409HWx2vwGQLT/CJnQvKVemKED+//WOkqLKRrBuaHsg/2RmYpSjxR+vQ7vDz+2TmJqVRNrQhQdp/twftSZTkCehJ1S6cw3f0RbAokC2qXIn6YrVW+UxdDiMv4her3wgDZ5euRCZrXTlPAf2tRpeGyR4hFjOYCzpxoLV6Yp/zvx0tkHQCRxYf9FsladoFRCCe9bWf8EhOA11qMq/vk++YMbHAnYunEy9VskeSvparQ8PwDSMu0tBz/yhtNWd22bkbz4nqYY3DDP263DEZ5zl6GdtQoRRJY+hbM+UUxgVhb6CCeBYKzKVv/YhGSm6ZXV8hUJAYPEmUCIZ0bjXdoFwQ/jV2k35XEDqhV4uyKVkN6y0VmvFgavkNpezbF4BA6wtb3VLyFSBu3TmBk5Zmjog7u5ZAAf+yTXC7E+Nb/VQZvhZNLIaoETj86eTPaD9YL1RHmvrdWTPU8id4vPmfoZuDF6+VkLF45LFgK+xAM/3MOp7KdEVXV3RQm1ebhR3kc1CXSGvrojcX2ybzzlk38li5BqnMdQXZplmkiku5/mPt97nNb1ykmm1V0yLP1fp8DSeu2z4ltCUVqQRKbSK/w5b/z/FfQVvbrkOEKMYbt7mQ8OYy+S2OdHC/3Bsy5abXofUSQ7DQ8x1WnlDRYRolraorOI2lShkQKY015VZ7TXwgYQSYykMTYRrL98Tz6I8fyQ6vcziiCoeZnL2Zl+Hr9Mixsc+RgvQ1VTPEHO9sLZ9RG69gc0ualVAlZGz1cIdt5j06wepCXoV53kfnR6ZsesCWBt0//h74Wvt41Ds1/wzPePapzSDmi4SgH74d7kDJwzzXBgH9MyfSthb0AVCa9L3teSHzYvFoWu4bcQ2dY0RK/EW2jXeS7DvjKQka88QR2/PNWSu2DZ944mrAfCoJ5q0761Rn3aBhRZP1n1SB5w7jGwNxw2djpqzpNtQdAm3dzipcpg+XVfNwwW8KjKnQ3CZrfBRztVsJxfdgGaX1crZ/9hknvnHg2K4okTwT0LDNXPnRl3lde6aN+hw/iaxLJVssYV6m99sP6J5bR96nlbTRhAaSkwzbSSssVvQSRH5mG/VKYOXk1inEOddC4lCRRmOEbY5+g4JKpmf9/BW2Pl8sbRXZAmVbGq/4JLAK2/6q9Qexx8fYd4F8V8udgXX5uuO30+AObLCTk7qg0DKTig34RRImmBflqZvHhQJM+Tvq3s846ewfnWvWIP2LIhfN47IE9SczeBEGyOPjOVw/425qKndxI0eoYfCVkkN5GkCs208JilTo+kHwL5ZtAoJFuC276X1SBNLFMW7V69Cvp6envGzECfKycNGYSxyJM9dofHsUY1CqzhUbWUrosvmycmlv2V3+PxiJwskf7mT9R7cyjYeZ3qoe8pGnLU83kED+I/HM7qA1rKBt9Eg2kCMuyJvrvBT4sKHVbLseocPgBuW8WuHGZdq7+M8IXWxJZvIrUE/7T0lIyC6Cv+8YZZ0fD9kIScdwOg8RL1tMS/FKE7kjxjadwYFx75h7jok7fz9Xx4xaXhyXtL/45oJEDHAoTBTl4DZ+nqMV4h018zWU2z70fnW0G1uSm/P3lPm5fRihtuUGD0f1Gzl3sirF5YO6mamljksWprtbJHHmnaUfzuRvoPx9emEacxYmmhn5MOTozueb9XCSvjlzmScpoFX19jTrpMqsFVARiESjDsswD/3SUWAMGGkE43O98YYR0HeduFKvTnl1x4JUK2rutVbRueNGtQh6oGZcot/GxrdBPG6+vc5M6P7rqRnLjdqcehb2Sj+sezGKeAciskEXXJHWdaN1tGzXdlZy6I2zx0ugHYv4vDokoVa0NICf15zRrD7TcgC3Ang58qZ9EqrfmJEHHFruJwFS+lUl8KXJ3LTKusePrXYrshLmL51G8NficLGtB6l3UrKhEDarr3EOc5YE0Z6kdpn7Dn/rPUi+6JAOo04KmGZt8Y4DMHq3AoXu8LNNHmWQVUB15a7arp5ADu5Pju5IbN4ZGYQwrUYlfGcHiPHPnPzNkMLj4Jy7WSwHZNZrAIeKc0t2mT5K6o9+KnmOAa2XbD6/que0M9wF0QLF4lrsG7sgZqksQaj2JWKbC0HSUo7wPEI4GnpscEVBpVAmYMdHq+q1va+3YOid7oPzhJKL6xJci3un5VR/PTQchfnCYHghGl9e5zT7sxNi8ajpLeVLbEHj0EEt+D0yNIEwIfGQj6igvZja91I4RnQrMvI4GEA5IwfolIs3lEwiGtUTXi9JQFZrO3XNXXL6+snUshLMjeKJs9xc6Zf8vP1TxgILrAydZRMSs0pdLQDWx89QCGrlFLaNKtwsskByGb3fs1eNh4s4IEJDnje2IVCh/b/quicBSbkvM+WF3ko1mjip2y4Qyi2+MjSYluehgDGKhIwKjoPTVpJBy6AL1v+i248zQiodffpEs5GSj/5Bbj96xJrZK09hPvEilYgBzTHVuNSR0iHLFs0B5d2aIZy2prZHVWe6K9yNY/ROK3ijO/7O3S6XvpzPC+xVLjKzIW0jG48stT2N8hlUzTwDZTOY3j/EjnFbGVAVuBSOU043blKIdZS9yCpMGMBIVBKQwCeS2AXwhPFRQCMBo/04bAmlrsp3Ic3NpSaLpNXYzkHNlnwxbkDugsWdifanhXe54AAV9bwkGcNpW6TQuosZA/ziaeXoz8YrOgLzp1vz9JjWTi1/mhSkKWBuh+fyrvYin64Zaoq6097mmhfq60iiRG5GP5tWyqUBpj8VWydNH1O8JF/8PUQgBI58W4I8DDxQeK9h5oXOJbz3XQfbJh/KV5ITKvDJGdNg+MVv+KUap7fubOExPJKGvv+2omIVRGjGszFmzpXaoxMA10LgiYE+cx62f6X03b064vA4Z9vnSost8mlUDpdxNOVQ3aNMpWMuEkNpNzNgiywlqvEYJciMmVHWqJk5ykWqpla6FD/I+PhhFURLpLPoAXs5Rzx8uIOfkWVuLOg1g1k8wQNbnktMEO5j+6gOXz9XmldwSvNr4Gveo0woWgAPyNeYnvAePw+0GFuTMjaTWPvLpA0QnPcVq3vUqKbQi0wdW9MYqx7rkb8MOh5TJKA4/0inu5DDaNdrNdpqJwR78Bco9T/pGkwTDHotqN0yDkwYsl8yqpVKbZjxcKjA7DWO9O6zDwJmcEZHS5C0+mfsYrayDdEYGiQVq6z9L8wNNHCAne6hh4WNivi+mYa/s405tmRutk2enG3gPxTe+0FYxUfYXI7PhBrsJ4QCXlYkbKCywc4nr5ahUBOmjxE4FH54QqHi04O3ibyhrTiS4vefE6yye2I9ZrcgHz99KoNv7wXSg9q5ZUmt+TMCijsBKguabHLTGQsJP50h4IDw9X3nWQ9mQe97KyJQ1ZidryYydaVk41KH9XmiG1ZnU2ZzJeBKEBP9NaRs7dPxEMSxbnLqEcNgPg5BA1lmhNtKOBe4xdqi0imYJRLZvYpa5egkYEM7eyh37wYYR5Wf1MCYY33Zyhwh/H4e0ilsfTYUw/xCVqAkz3XwAZFXXnN7N2HGAFiLV3KPtmvBlMP4OHNqbMhc/1Cg9v0H7vNNekYYsqZCzmKAkjWgObokncAirFbkXR2tvyyBghmu4KVQtCkw16dJLDLebbEB1AEpCnLLNsmhp5WvI8JoSlmgpV9FNyyC53Z2Bd7DIjNNT5NFtRxAiTsd+KQpIT2MqyDuvMQ3XcIgMZ/6iaLLPl7MY9xtkii4Kt6BCnA/8vBlfMYa1murWGTary5ZFA4vtdksMD2ObicfLoI341ljO2zHtcm4yHAfnl6P8iC9RwgfvKC/+zBYwcR0LzSjb5db5Nl03SlijHu6vAo7ghDd4ON16C17Ih0nfTM8rEiIgspT0ay9q/mw/b64XPvqlhLmDGFZ/MH6us+HtUiyN4tvVD+3nw89oXnPszzvfUR0I6QTAhN3r2tOVnS41fagNcgYx9M/gSTkXoKIoP+t+TmP1JBHAPszwUZvPR6cJOMMhaa3PSfVG1eswCDZtCDrmti4WH8EgjDETcbAe9VBTIuxMb4V3O279P7l7rRch3ymjSGJbIbYZokn1jEre+tZBIyBHJLNkBYZwvWT2An8p4hEjbQCyc5cURWDvtq+7CVf3KOQfppLpofrZlEEuNEeOSr1Qn17DboExw6wERmI4qxpqX5hrd6xsznrM0953yvpFXwo2tL7b8YJwragd6KOdPdo9jX0YrkWs1RQMKfVCk1RRM+6Qny4tY2AvqndSuw3qTKFNjx7FpZ2aBTgjolmuFl0PALuvuCoZMYaWmdkQ1Ea06hhhfTha++eAMxpmzed1DcOWsuHTXTVJtGCX5PppbzA+UUIEHIIaqYqhNTRH0jZXunXKrhC1IkVa/kKjO0O+JMnUGELK1I8Gi5g+4uI4KWZg+//GmJ4ledD6p6dXlvQ04IKUUW62g4HTU8BDtUYS+t5dz7YIrL84bUcObU1S0Uoj5jdaW6nmxa3uHRM6lVmMdNrmHFxSt2rG0w9qeE/K2WzT7QNnKEtVPqhpi5mY0Z56Q0FFB6JVlahwlweRBsccocVY1Q6EP0vC4jZW0WM7jSQx7com/fVCyqvAnxdyHb6pHjUB5PHT1rsnBBQOTVuh6rS9ouSJvDtBnydD8A5A4Mskz4OhOAJYwCwxr0RMQJt6kW6jMwYDeMCdJmXBsbGDKiWTPedg7cXte7pVetn+LvfBA6mshETP61riYFuIl/jHSvPKdm5Q7oALpQNbY+fNZKiUHcBG79TYBYiV/wWnNw+oTxk6faOGxlpvlO06DCrhzElaxvOzYPmSbmiN6jGsvqvPNbRxjBM/vcXWRnBQZMFRu7Pgw9pnaY4aUBfqWLMBON1oeKKt1swjjwUYDsu9e+UYhVfR6a/bBJi9CYlSCx674loeCmASajEPvmBQlr0M7R1mX6guRYw4gdAcsGZwlqDsas/WzHja27Zok+WVDdCsWNerLuOFbqTBr/hxeF0hXVDa1JOhhBBKlRBci+OAtol84oMcrjPhiSyrnqeoSEHlOdSgelNrlfQM2k8y+2G2liB39ozal0UAmO+maYgtFDifJ6fT80cGwbq2tUAWnvFZ54rKpXUIg1g0WFd578Y8FEEnp3YePA5iE7bBa/811jqu2i/862a1WaxFXtO3m7oaWu3OwCtD3cdg9fFqasKof4w8EpwB4O5lrjcJ2yoshYZxiuKxHEgJV0yoZL5PzE98yz5rbTWUoDJmjmIbtJUO8gBgPB2Fl5Np6bNF/ZyYLzf6GP/lECgsTi2O54efukxZTanwBX+nXNcW7k88KT7fo6cNrVt3DleqPm5OVC5kV+hd1KB7lh3ZjZlGMHqvDy/4dB8A/txYF2o3gHkHo3FE7WcwZ0GWTIaC1y4WgQiJntCJei4EvfIS9QeP11V1I2RkkOkdOH/Qrb6L9RPqQBTneIwcnnJtABTaeMpMQGzLalwcIJKI5DTqnXmbPltDOMxi5w2+saHLMLoVlN+7SbY9hbbdaoOLFwvQs9znv12ABwxZhWAWa8RMnVDpGhkvZjXULWx7sOk/DqySXuWOQUMsmdM3hqix6ZtjGiDa0oGfBM69bzRaYO1pFSma93rq4uE4mLCCODsWbkLCIpUGMuZc+VtwMlHPSU/BggVrznsa7UveSU9Ia2KB2eA7ZM3+DO/R0HnGuZ+q9W1rxWhuzZVyBmFgcFHsQJO7lMWmau5ldwyCaJzAIzs/PA2ZYVHNqCxnmxpMBMG9+XcqKkfUdzM+vChPH/BKQMGNyBlh9tMlfrT89pUoINKkfPiooXU8OBCMsYGPHetNlhO6cKc6rM+bczE2tIpZlTuVcliw8YQH5Z/SaXW9P268LqttJwI6ZR2IwghD+qADyytPZO4oNu74FlqD90NKR00JOmr/mbcXFoxb2ap9r3j9gaL3xWLHTEuypMeCzbrbx4hHcCz19keuKbLUl6B5YOEy61uDSyudWfrxWvYAB/RzNPgKHiqhrHSBs7ORJIN/EWpFiQl4EKA59RId3TRjuKT5pSDMXLKR/tj3n+T3dOZhXVL1b2eDAIddcrsHCvAG1fc2gowCWZVEQwvENWpUtljrezmwqwDdHtQaNxF/QIMaH5FM5wrFf8NnTH4MWfiAcLWJ+MbwH6m6spjAaZAFmXvfYsxylVXARCyLKR1+rCJn2l5aeLoLjjj3HlR2omNF7LSzkytCY7aOVVLrWXYGE6zLUBa3JT2IufRMvfzUrA5DGX2jWSe6Pm7z0XZb53mZdkzk1Ei4eo0/bZMxD2gIypHJyhJERg5/6nRSSuiOVvtgqeCuItJLrqRMxBwP3FTaoxdgAHcmT0E5AN0YXd4+mmYFSI+sSlbf1v0sWm89DRPByWPtUA+BkuHn5rNYHMrJwqNmj8aThCuJ9aAolhPasZkbVageWL9V/dPmmJua3LOHLzNvJg6TTxJ53n0zV4rqZgeehxdvI3z0SEQKciXea6wRbqy5xMA6CVZXD9YiyPK+qLy84Vk01kmkSELmz9XWo6FIYaYqc/UzDi8M33dYltIINtx5ZFA3ew/5l55qvDuAEydIM78OxvUrsfoezw/h1FCz0ew/anc0pVa4nCz4N3Yjga3U4KWOWG6N84DLqcrsH0KVvb/BAI1k9khi5MZ+EiFYDBXWz73lUuXkY45MRgA0DM8xw2lyVWV14pK7ouI2UyDQJjff8vylf3b1HyrcXTF7jysNmLomCXdaq+bF4Tfa0yxWFYhb3XrjP3S5iuJ1jHW1x7yWNV2OT3Lqgplc+DNwNLg0xrTN5LLaa1lip1kdgHeyo1tiIEfx34WJd4w3BWa1MmRFyvSz+sZSr/2KQhecA4VHB8wqro2nTYCQ5itvBK6/8taOnpxGAf+l8ZVJ0PoZBexUdZ/bJ+QkpSC3wCr1jYhb3HLLO2GId/yFT25FpBAYZn1aoEn1bl1k+qpTF180XlfToD1itgXClQMXsULx7UEcV83s7+WELatyVUY7bmM8WF95ZnEVR8NFBraFHOHVyxnu8jGGjPlE5IvfQQvqFKnsTDi5N3IXxiEBCx9YlbkXpfG9XQP2GHk+ukjLy6pdxpq9xjz9UsFlAQlTO0SOc5FLMq9sVg0QeWRD/WYaD27rxjwATtZn8e8HmB5S5zD1esqSYWENCScHK1yTPtd3uIt3g682roDbxFYxaAJZjL25QlLOJCLGtxW+Q+cTvkAPMWXgcZMj1qBhH+imOXxVNWgpRs3ZJxaW4TugiWaSMPDR6pHyJX0rdwVguRUZxwnSFvzxUse0E9QgnU3B6X4csXkVq7n5nDMEEePJ4fzUowNq8HX0lIlcJ0kMX2mwZBBfyQ7q1TwweK3kU9VKYJhdcEAO4a5GqKRY7k72B/xQMqLti8FdeXTngCgVdBq0shDhtCYjqs8LzsL89/6ZaMy7Hib4Jdp9YSE16TO3Iv0BHUMLwAWG46XlrLOX1vs/TtvplScrK/CZnxiNGr7twlvEayB+f4sAcbNqezP51p197STh2+r72imrTgM0sFPuBY84UeKyZYbQJl4ec6fKfNjO751dC9MUu4+nRDh9jhDF7OUR25mvcmtt84IJMtunlUs/ihPX/0zQhCHn0RGPa0SF3BcAzWXr6KFzYzTVq2KXCeDSbDRJq/nFL+b4PjJm1sNxC1T4ij/Nh5c0vfA2hdt2SpD9rEPqIaTlLtKHUX4/B15wAu9T3QlPx7UJjuC0GzmFnOFgKP43vG6Xcq1s4alU1ZzaZe45UqiUcSV6OPN6mnxHE2X13fmJzF64Y7rmLV+9hSi0GUxWsIEH7xEXFVWXXfSCFnPp1UVd3snBiVfNMvKECQr7ANwA0+pli8iEMGUJlo6dzFCssRz7Cj4iAoTjPulqHKN6XEEMYzRzTFGtO53yvCeretSh+3nttqcHw52KZe7ufd/wKS2K0y72jAB6GzB1eu0cbt9vKcqUkFAWhtsMGccKKThAQKsYVbbMXkvu4WwThHXcuu1GUvNL0Tj25xj9+B8Kuxk5gw0TSYeFfoE7ErxFjXhKhbDg+sG02quXseZ7rbRXxRvSh4ekIjL8eROXDx8f+EJnmT4LVS/uRk93NEooEECIi5+Ory/yGeqxR8EV7Y3NcGyTSkqNc9CNMbtMNpHayYvVmbrFH7ptk5uz23GM8eravKQ0z9c9VLR0k2NhWcx5ZzmPnH4jJ3ICNctOITvpGmx8s+6J7NJKlYhkOHZFXpwQMOXl+XcWlQTjl4pGdDZ49eplcWkXO+T+9ioLbOSQ7/4dz2imexPS8cH/Eb8m3yoicynBTYUc01A73chjDIPYuvHAxFd3VVQOLvdnA7yR0+9GKQRsaYR2D3XCnwyD82Cu3WYBVb/2zyVVPKL+anSe3Y6oo9CtYVzk3deBJTmVeL0JCB17T6uJUPJQwT7dwcIcvOb3uNifBLc4ratPsPeds5xpIqtEf0gPNdAdr9RTwAfkYxvYbMlgaxKXO6b/mkUthnX85x2lEs16GfCjfSJJZZHLqKzz2nxwVpbVfog77hF/G63gjQNH+Atw+ezObtR9c47z4XPEj/TlJ6LHM+mDQ3A+ELFMfMeH50w2O9mUK5mZbr+tqQkRAxfu88fZxL2jIZbJNlyEfmD7z7K78huPP5sedK44Dt+uhF+J4GM9CsOxM7nvwyJVQfSorj8UuX/k4J0qzk8xN4Ae6j0v3W/ayBhDlA6Jd591EdrmHqp2o5WdaU+cuwAuxAKoAU1vTCjlKGosIUAxgBODZ8b+PjXA+q5n9lIooseACZVlS2tPoIBTt11XIo7znlIjae/tEstR8PFjOI6rRrQOOTWq1R4jU+HO4qe+OXDLGq5dKkhE0giSiZKwOfiOon3RBGjMwtkrkbSHNctaqqG9b5D5FU+liE2WoRz+yjxi+yR1kFT1GFNuDTgWolcyx0qNyk4EzfwELnj/dbSons24XUnKVgcAJfIWO5NitMo36fHkb1jVsUt6acnQLJDYin4MnZO5Mvu7rSDdGHob5j+1P/N+cGs8jIB7x7XxoRvX2zwJvXQtI1hu2kRu6wafgI239M3t20LGLrKC/sW4BO2LDTfUtoVDuPVCRLUvOK+PlU+xhT9+3r9KQ6fqFnmV4X94e8KGDbr0wAWgYp8mQRIxQYXOng30uNXvJUQgdRG+E0vLkvWQvUvKLCFUWRem+0E384QvZ9NSVvtsWFlEE7kwVoX2phZQ8YmEdV4GlnYGSh2xMvWD1Mb7Sk74N31/5gNmOmDI2P7OoOxzjazo6a5tyR8SDoEDbzGVClaWEP6Bsde9KfxVclpBYFcUcjcFLPQrOOeJm39PzrPvfdQEIZiEqrhCXkz6NQkJ4YutLDgni+UJhnbRbtcALOx6Wj8F3aBJqXCSEIiyJiGWS7EApAqh4N18cmaWmjo7FmAAHQHl9VdE3iVcNXBJfZe8TPuNWUsCxrsD/eX5vXMd/+22BFUk3a985cYyCFuaCMy9aMf1NxAeScbOWU9bCudBFbmnaXFz596Lb4jll41lwhiosgrKv3V1xuVcb8gVqhsxp9JF+PGPFr8bTfyoPDGXbqIHtDd5mVY9jAHEayuCRklG7P62i/DnBuAQvEag5p2Se54LoNi+qNdSdR8t7brGZkBeFxZHFhxO5ln8AuD5Kx6D0h4MtZ/4m5v/Q8BxEcRyeQStBVP08QxlxPuzZDRXlVBr726UqyHjHm9+dEJoUjdEXjwL0BiNnXt3ZjH/cx/0+57uu34hHq5ucZO1ElI1DBLBMkYXeKk++80tILGFAXE4NC3H51hM5ZBLwgVU/4lgj5reg2pEs2V13VCqNKX4k2pKqrMCaVpbzZaLKFmrV8l2eGWaSTtuGN6hPBHH+ZoDil9hICNoyUI6lwYz1ED/Bx5SStTbh1QiB5di1a2QN0ZyRFZ5KM8x67rUf8e4gHlZFozWb4JM/jedO5i1v3ED0Hw0NHbvuZ79tuH47cjO/cy6F524lKzCJnDty9p47/YMnwgmssNSoLgnA4A6P9VauUAMFRp2R4bIwFb2qi7k308eTnlXAdJo4nZrC7whiJoenc/bARkD4uXopG+ZiZjDSWkpJ9KW7FPyab5WHtic6HpHiBdYobt1AqOgKkJsCu37Y4gvj+4s3/bks9puHpoDRvxZqqMZq3WlQP+yJj6uL0sL4lPuTQn6SkO/VPQvroY4A/xonWrO4eya4UwX7FWM3wubPQ0LAoBIC6reaUYJ0qwdPoWR4LNykoJO/S0+0r4/BUfjgbeM8cMNiPMcC2DMPZUqACGtKnu5OBU8cR5fdfYaYyIs0iZg8FgMq2NoSnKbWL5ZvJ8EeXtaGHedvBaKYtYNmYmQUQivsAUGDeDM9WMNzakRrAudm8G5HltzH1zpv+N5EFiatnrm1fZXgndO5AQxIVG3mqAWzxo3Kz+iLdyDsWq8MPEkifcXy2+V+jsHs2h/+KpaScKA39qXrcRt43Vpcv+wdr0HvUfHX8Y45C7rwuOEddCnuIrFWUSsgFdqDmwRwI2YPKvHNeAniNpdYptGl9QeT+O0Z/CwpdQK/gVIqG+/HG4kf27p6TuBy/ppfaX2hFdmdTbMmAxuW+OrgB0onzzeDaPhSABBawco8WG+dq/IHyj3klbUGIV0OKPE5XfhbKvym3cN0URVUhnm3FxYTYuDNbiqvNX0h/gdIxMbuZh+Uwsrt7g15/ud3R0IF9uW13TMFNpe+VnPM4Sm0/mYnzzQKaGYwPiswZqQqxSgGlWfS0GOFhufKREgVV/L8RpZ131fspCieoJRwoC1+oBDKOfYs2RyIkaP/cKskegVCiW7eJABlremm7imW/29GKRhvEsGFKnEI6XtJsbdFPgoxDMJdgrHFxsAqGQth7tWBCC4CwTv1MQFgdtHbBdxtxNbBs4T+5hvU90GxqaA1CWXpHzI4/ueyqlw6O+5AG4o3mbarWx0P43iVF7qfCBCY4gN755X10gyi1zGW+1qbV3VzpaGfM8IT05qCJB4rXTt/yHlRhGEq0EVWCl4LVNoyBeXrUpsPcvklu0lssbxAFM/EPtjI+H8x+itMRAUN9hDELYaJ+tkYjoS3tsyCivSSj0ZDp1qWfQB/D0a4CUvbRPw5e7YG7bZK6j8cZBWrWS6+M+yL6u1NiICe0KrjaV0sq7cmpf3rKnu0b9stee0NRb3GjjogIGHc7vKOcFW96P65qTVz1LWzi0BRsvXuZB9shQjU0tKfKDoMjC/JahAWPKkGhfDGIQSniuK/uop8EscJ1fRhgv7+XVUODH7YCYm57e02q1Z9IC63pCLHEAax/5J+wJ39yJ1EnItfsAoFjpM8nYYsFf6LChrvIHj4S2dGRaIGnU64dB+d+UC8GTqemvKV8oKU29oPxUOsGJyyv1LRg0YN5VUC8rgTIrO/21c+LjOjPr2b2xrn/VLHd4Ftf1mfR1KQeQU1QyPu6UUuvWPjcWTCK/63qNfJgnquTHSxyKnpiWfr3pDiQp1XIwzs++3WRWw9dxGd7nDCnu6BCmt+cUcpe1ScZ9OjS6l6VU2S7Gx6SDxpFSzTVbp6SUDrC46yWeiVxGMpenaQ+AIz76aktsVwvF9muff4bRB14+fogL4OKvoGoCBIHls03X/38/6DN285Atr2DEw2Epm274Nyfh55Hd/Kg5DdZKsVrvSrYddBbM1O1zPmr1TFc4+ROgQhKiG4Vp4lPRiCizzZ38ZcMqdpHBh7K4pZq8OyHzTSxuSNYX5rWnfEqB5W2seSYTj2sVRbwht3Fd3rAK1LaKbY/c9GzNA4ffKZZb8UvxhDgkjyws39G3a/RQaABLly2A/SJ87s5YGmRGE/kXTU5lclVu8pFPBEKy/0ChnV5b3aRNPX12jPvPdvHGUJFCAJ2+wXYvwDTIwcYQwHZl2+8CQTKLgi/y+NTX1mH3HOMIi1OIjVcXP5iApwmjEEkTOOz111ks2FJsHB/rRo+CssR0xaaAD8QTmei26WcPIfcqg7Q1mJcy3SI9Z6+MHPeVYWROov8DP+8QxekwghGDIwXOoQUlVnoGNJFNqNipy7s3HxzD3Ep26sKdxnalzwyWZ0iEpZcmTBmGhjF+u9m9cXdYQjkUIc5uePOIvc7YuZy/Hl8gUdHetrVUcmGaD9c5d/Lxl5eKnsIszI1HvBVOomaaYcwUnz3UQmKLjx8z4Jmv7S6VgJp3GaY2uIePZPD2+Lzcv7Q4Rd4hgJiz2nOffUcP/5C4VpmGEHhNrUZUNOflSfe2beKLnZoACMKk0Eyof9G5aY1XJYFST/WLW/14zUSPqwy6dn50QeTObHI7OSmx/UMbO8CjDHbMcOEJ9QtY5jzEMpV3FZOaq+U5dXoE0HrpKZQXWYzi0vfZgRqzTvOnPzXkI0BE6oYZBxq0u8L6TrSRZEYYQe8gExF25wdsRyY6uFHHY3NA0nXbqouvB1B+Evky0YbQMARy63NrP0KQW+n0SmOavYfOszyBbdrYpQyKGBJ5pR6tH42dHpQpPqq87DuO7gAlPoRLimYbzXDtQM5G4BGc+fjTLV2FMo86ElipKQ9kcpybm1qoiO4GWOVN4wNC8kffq7k9zeozQ8BG8niMsN2vKAmncOMGwLp7I4yxRH4P13QiHcK+oTCkD365CsIJrN1BwqaGRgvDeHRv/LvTIQp12e7rveiiXhHBNWeVNhqh3mlSjWFntsL6BBvx09f4Rck96E3QSk1cB6Am+pnD779eDtzkwvMIznRMTH1leuCDenKfxOQFSxtt4vagp0QQMWaA8bWtdPwXZcaQefYXBYVAbrJ4Um8kOAgM7Qfp8/1Zp5FSuymDiFoFLWW/sVLlXDIuOu4JSlVdCqy1jrwV6cLguPKIKIACRZfbNIU/N/4zOqsoCdqmO8huXsNiS8VbDWTBHop9OpZ6Y0gUW4A3hPZc5bpdbpnbDGlftznJtsL5RbSCBAbPqKYePDNVH0JgHg9cGN1nZ7Ado2b5M6P/DCanKtckH4pFp/xgRMuFBDeh6Huei4Qzz4D8xeQnt2GAIbzFxGbUvUuhzsPQ9pz8hWYwKXsNaeEVsc99gdNspBPrbd6IS0z8GWtd7FnHRuJI6ENucLQ9gZ0HScnPAmAHW345zbZxIlHhv++QpWrtSG3PC4cEe87kMymsHh9mmbc8PIpFO6UMvqMIz62so3OVGpdaMRAP2jZazpZ+Xevmv9rrfXUGDH0HL02VbtAkw+69Nr/3NIHfRh4Af2aST+sTxo829EMEXVt1Y4tNqTC1tgHWWhME3ZQSAmZJjC11X6NJRW0M8VEXBYyiLwgEr+1PGrvWggqAZEmR0ZwpbZo05apSPKiZvzbUxaxG9oPFh6319pRILf4jQacIftJh70P9OTewewDDKqlq/m9r5MeABx8HoKDWmJnCWsFgyqd9ncffzABWTTpUn79yQHlYtrIAz3agou/6EuciN8NplezHJmVAlfe/wDeof0g2WIOX7bqsA/L3f4WwvkKDpP5SzWrsqDbluWBApcJfkdgXS+7Da5txfVzKQVZtNWYHvhwHdCjU/9YxacANVExqjzTJoS/JaSFMAh5OINpZc5JIVmFyyJhAGzQ231cFRWlNwqTbNTNcYTvE+Hbcjve4Xyn3WWgSJWNtiz1+b6m3Ehgn3tWHPSxsSHP8BxyPQtWX0fvAfsNwtgkFVPy0cKFYbFb8ckSMiFqX8slu2+q5IOz4RCN5tSKYT7oW+lEyvgYGxoFSmn9qwy3xZL4/8/i8lOaSU8GVWdL7xmS78cIWLftbPSXrcaXLsciq+ft2DhMcp6O6Pj2cwP80JtV0Zm3PupyDqYuEFTTS2tsf5Pjyp14O30NzklO5wKfTdrWE2hPfvHM+7D6WM+iQvLQdiewYpbtOIy1PPU1sG7mWARyw2peIdvlPYCXSfwVm/qHOPVH9n/86b7ZMwNo60U1tmul6191NFnIxg6Vjn2Z0Ued/gbG51o5xP5dr8qO2erfjMRpKmlO1ZsZyYeVota17p/zctms7yEeu7myI/DHdGcV2dJa8korZAqbwS2f69jm9Uf8w3OvQGQplb6WmHO+KOF+CM4rHsn1OjwN9/9+HjMGLZHs9ymfueCgwG+NYZ0SEwBb48agitDl3Q9V0iw2AsGYSuIaFDvvNMnL+CZXenxIzHPa0YPNi+HNbpBE2Hnms7CcCoNeIiBrMtr2eyjwZLQC1edjFwl0I+R2FT732RgfhkdpjdYZZ1AKLCdJ+ePITTPA8pC1bmMuZSbaS6cAaNeAXw4aRXq/EyF9m4mjiQczpPOgIB65wttQ3vV5ImwOj6iUtf6L3ENQalP4/pjVUpxCAGOSwNqfFxrhr54rS3jeQXauwdJZ4crUwaIeZJFAxxSES3CA240Ak+bFh0Zpwy0b/YWdyFnZgtG03JZ0Q2+eQTViGU4tm0EFuzYI3ADcAZNaCJd3IKzGA5JwifwoQI6GaS3YzZxlvdIVzHs8iPU/KDdeE5W1CTmjImq/x2PWIrp9lQ36BhX0OLj70CrPxiegXQq82RXNJ3R14eBcYtEYtrQmYH/mcjCedmS8Igm0jP4FRSITLRzHZwj4SnpR+Kpj4WX+7wVM04/AVFSCIePAZARVi7u1Tj7NTk4hIPEPu1qqe/o8fcyrJ6EYqEmbKfIybPF6bfQg+z3k+83aWlknExHbb7V15dQxEHBAIH5p0MQOEkuJFLaVqx7kyeIpfYbhyCLQMtm3G2VRqL8kLMWaursSQSpjzpOVRhWYtDZsdA9us2zYnCrSUFRsKXGLAp/XjiM+Te99a8aVTA6u8nJu7CVS27oON9y8Ay6DNGzpBLXpqeP4vp8/tlQipbuiOZMGU5OAj/ng8g24bCkD/CL/TOD3KBpiA9vdiHQd3tFE0ZUL7xSGIyV84Yp+EI2PHxlycdSeBUY+FLhxMD+sm6WAvOJ7C4M4cf+XAbGvqsHyKgjUUpG9hwoC4aMwhk6xHbjEPgh7DQdk59QJEYdLcxu/Igkn4G4m7kA7lqQnolWQ7zmNP/I0qvMES1Ugw/fNdDugALiVZMKaLbddNWQ1Psg8rZci8wEy6OkR7k/ZwhpHYhvf4oF4SnZwwj0h6eoF1kELwiphalNib4TxtcsokWENJdX+oSoEZboJqT615w3LrKk45hv/6pmJj3tEMh3qzBJoyLtqMQ4V8t7P+w1xp58cWMShCopsnwrZJk81GDx+uzMDiLVWp8cZAVhGeF9pCLZ16N0I4mmk5Q4ongysjDnv0ikoHDqhvLPUmiY5gNTVkif2/UkdeM5oX+nQvcewGATMvBA2ctcCkwxWCZzyhAXg0Vxu2yk38zIaXsjDEXdrJJTYdLfwMcnKma8su/RQ/mGKLz5EpaQNjF3yU0i0dxYzCktYIGPVdIvZaOOMGWTr3xW7Ws9ZKrGB3GnsC+KGy6bN/x2oBWD1UWV7YtS/P3urNiEaCeGnqPxXGgqqyYLOPbmZJyHn7Inhd+zVZefTyyeuAGiLFaYrSbKZ7BpeRn8g2WGImervCVEPahMXIb/uHBlaxGUTSKhiXFVtbi+C7Isx4aAQ3OS81SdE1jYkWQKhPq8lUtIU3kqwQIA2HBwFeBWq/r2QTxfGPQoIzR+hjDGJDM6xyQDg1VSiwPPDDDLkPMYCY7m5Pb098rlZTOAdO9ZQZCts0V7D1c5Qv9zfw19LFvxgqFCjVOMuwEQD60CpwDuCDtwNbWwzhzSxTAyO3pl7G2v+DJqZaEZpvxuIOpEZo95UeY/Fntd9qJycYh4g7wM3wogEYtP15V/JkRFTcQPnhLVf5KSGagwBWchLCGxcLQKK6GfTYb7v2hlSTdUUUV4DEFpLF4GWlx3TpUA6d1MA0ho75X6u7Sft3931SgGuE/qcs4d3fgd11hhiD+oIhG2t0lq2FPkgCGOpIOer2lFC7Luk21nrZLpO7juqJJg3JsEtSBZIHxQNjeyAIVGDn7nxGclaF9nQFjMikPR2+/fO5cF6diog0PxO7pHscDHcHDq9/8jXvypN2Q2BdyY1W5lmVHQ9nh5Q6djzl3thVVynTUL0nxDqEVYq35v9aULinxaYfjMX5jwW2I0HIQQpbhah4565qml1aVTN63Ehty8X39S/oxGCmDhQOg0CClvtCU6lTttjtgs4QUT5JC3CfhGIlcsS/FFlOrKBJS86pQQYW70bzwLiZeV3E7D3Kn2fqboAyb2AoTB6i10Y7M/UpEbWx2d1YtUxGumdlt2ADfz+Pz4JBwtlBhcvXOYo7P9oZjYCUjJCpxl9Jt5JLRxW9+BYmJA14Pi+eyVCX92aXzx830JQag2tbSBuoAzoWCC5z8MEcAVVNs9nBcwK3LggX3H2gCIKaXXRUNPPFa+MqJiWemgCSx92b7VQYxfUU9PFA/Q1lG6IF48d4NKGcJAvKg2e9jfckfxN+zm7wDnyHBUsiWidw+/iN2XOaDFEV//iufdTdILPKMq8Ts+TOteZmHbRjBwZDSA6v6UzdssnKAUQduCZa5cv1ZKNexuLLpIHsFlt3oN2YJUXJ63VeB0CdgNh+TT7EYPTlJ21gUDBOxTc/6dOAlL6SHk3RgfgqEz5NENhPJrVLQfw09Qa3HEyPg3hXixJWDIFJeUUGaYxx46uyu/Dn/EYZnpIQb1c2PILf8hIuBRqRp4O7uUlsnQTuR2fVMpyw8nX2X+9JgBEdADdQJ9t5BW2WlIJa2n+YjKbzFZ+5Rplpl1eRica+8b9tC6BR81oobHGaMtEiXsSonMCdhjVf3xT7n0weDdeldj7SQ7oBvGjEG7akjMdpKNgdf+yXNnCD0Wf38kQmA5HZmSq9nTlDCsWmgkPON5kbdYfKQH12W7/0sgbejd0Og3qSqZbcpD1T5o6SylR72yCBD5h9StWKkAA+b+NstU7OpJ1fAB09JnZ661OGAFBZBuU9nWfGbbh0CkKo9hh8rm3yoGMlEJj0T2ra9YULtteU5+Vl0VcQnF/iZ9tTRqLdIX37Dck4wPdfr7g0m2rO1eIKbFJxFo7635fYLZLC/xGsf0kADJBnzPLjpUKQg5Bo9wMta7Yyp2K2mjamPesIXgkDCpcu3z3WZ0qVV7ILA79u5rzF3jac6HP/D7NzC16SGKUylC0SrNOpDTRGAWq+jNcRtdLt/nywN0aYTWHqBdH7LyJOvfiLUGdUFTrIjUyYcCdIu+vp5TU9yjhpgR06/5/GTc2Ucfkgzaarmd8rZKI1qCM87n/4ZQVjLRhpmnoNlkFvINyHZ3R1sZSR+KVhnTK0kaTD06ZICWYZlEnBYfYw9uGalY7odjTW6VTd065PvPUROX7DdCyakbnzMYqOtFB/saLYwpidYb9NmOO1/EJGqKY8d7aEjc3bxlLqdWiVWIB4chTsxZm4Tp56MCqUbqb6Ywh/DL0fMd6EmxKLcFk7gFYBKMuCKsrxhSh6M7rh1SffsQvuBvGRNe9TYQrEk6FUzC5UWDABHZLfr0HlTQzVzSx8lBgI+NVCFDgmJ0vjILajqDRwF5Wfha3odYW3YNMp1Ez/Gyu3taGJGfh5PaZq3IyjJVv7VXvqMOU6mqlaWzidFnLpCCsBzXsQeRei7JcS7MAXryn9nyqzjdViUGJRl6E4zdrBEEUL2caXtta4qzPu5hnHmZH8JTzVkaVbbQU0/tXROIW5rSovoVQi3a70ozGHg5KA0X6t9lcQEVLZ7aiKeix4gjcl2d2kO6U3y3LIZAdAdiJlNtj66mHF/5+RJ4ZRKU999gUwJW5G2GbbDgq4PBKlqV1j3sfqaju1qpB/tp7Qzj21dCcp76VqcVL3Vfeud0/H0a1czmqDG34MlKdx+V/rdjgIllW8ZSKZwVBHBYRdMVkCdde6kuzEAVh7cj3jdUnXsqzAAo1aQttg8g5jjAXEFO3C/9ujTfl1ukLeVw8zzHxH0ki+S2GBxrk+yJ0DYr7DJFk02QWCkc6JX3h72B5O0KWiM0Db1PpBFHpSu5iA7Ns6RmXZ/Ndn3aqGEQsnMzuqR3W70gpES40ifGxBHaRwI9Rnna9ZtW+wy3xrqeXvfJE5TrjfHGr5scJGAb/poG1Hdnw4krMuJCYNWKTp40fVvK7kO4LCecPxp4CWxnFgm2j1tDXCpYbFYMnSAdwPhBqlclmlCq61GWLSfHyd44cBoALlZp8MWmzokIO7Pjglkk1+ZEKgiwjCRJTQva6m27lPwfjXTfOXKcDgk5/gcNBDIDThu9Siw+hSCVa2fznTg3V5WdrQblOBopmJFsgwRUmilKrm+0qh/u2D+3tAUcSy50q0944auzjXbUeknfZ2T6xkeb1kwuDc/nWL831pXBxxLTnbQmCyZRVNEnjF+7Y279qozMzJSjq6yI8paCANIcwbehvwyJ5KCayfEgY1szqFsCU6G0j+Iezv+Ig9dZ4qRdR/Ixt8H107XUFJ9SlSoTLWCJNla5IqpD1BNxjFboidUQ2zh5Yt+Az5I3p4oGk5DsDrRLgBvSXnJEGpzOcuuuyTtv5hKYTezOayb9gd2NONezlls5U0Is9VNov9DIjKccaWu+s8pKlsQELJQLLTGRKRZs3MiCiiWGdIW6UEtkqDsngXKj1Uj8p3bmNubeYVLpRliVhe1if5YV2tNxkCYV6e1ZdHaVWN0DsuOTH0uxWDzcFL9X6mIWCCERWsZiMoWMAH78I5d4sP7QZ6xEHaTuvqIPiPkTp71js6QyRU1TQE3HYsE+bEtRlMohzuwlqZbrp4aRbfD1dbmfsVFkGLPifm4WUVRnEdf91x4loAL15cZshigHNl7E/sisgcQ4NCBycJSu39teYaxfP78MnO0Xqg5kfS2wD0w=="
                , AesEncryptUtil.KEY, AesEncryptUtil.IV);
        System.out.println("s = " + s);
    }

    @GetMapping("/getCountByHour")
    public ApiResponse getCountByHour(@RequestParam(value = "uniqueCode",required = true) String uniqueCode,
                                      @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                      @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                      @RequestParam(value = "startTime",required = true) String startTime,
                                      @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息的统计数");
        List<Map<String, Object>> res = contrabandListService.getCountByHour(uniqueCode,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 获取卸货口
     * @param uniqueCode
     * @return
     */
    @GetMapping("/getChannel")
    public ApiResponse getChannel(@RequestParam(value = "uniqueCode",required = true) String uniqueCode){
        LogUtils.info("开始根据条件获取信息的统计数");
        List<String> res = contrabandListService.getChannel(uniqueCode);
        return Result.success(res);
    }

    /**
     * 获取IP，sn配置表中的所有信息
     * @return
     */
    @GetMapping("/getLineCode")
    public ApiResponse getLineCode(){
        LogUtils.info("开始获取卸货口信息");
        List<Map<String, Object>> res = contrabandListService.getLineCode();
        return Result.success(res);
    }

    @GetMapping("/getGKJheart")
    public ApiResponse getGKJheart(){
        LogUtils.info("开始获取工控机状态信息");
        try {
            Jedis jedis = new Jedis("localhost", 6379);
            String heart = jedis.get("heart");
            return Result.success(heart);
        } catch (Exception e) {
            LogUtils.error("工控机未开机或redis未启动");
            return Result.success("false");
        }
    }

    @GetMapping("/getReportData")
    public ApiResponse getReportData(@RequestParam("imageId")String imageId){
        LogUtils.info("开始获取上报信息");
        String res = contrabandListService.getReportData(imageId);
        return Result.success(res);
    }
}