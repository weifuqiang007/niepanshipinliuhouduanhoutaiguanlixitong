package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.common.utils.logs.LogUtils;
import com.niepan.modules.zhinenganjianback.VO.LargeScreenDataVo;
import com.niepan.modules.zhinenganjianback.service.LargeScreenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: liuchenyu
 * @date: 2023/3/27
 */
@RestController
@RequestMapping("/largeScreen")
public class LargeScreenController {

    @Autowired
    private LargeScreenService largeScreenService;


    /**
     * 接收中通总部it部门传来的数据
     * @param largeScreenDataVo
     * @return
     */
    @PostMapping("/receiveData")
    public ApiResponse receiveInfo(@RequestBody @Validated LargeScreenDataVo largeScreenDataVo){
        return largeScreenService.receiveInfo(largeScreenDataVo);
    }


    /**
     * 获取全国各省的违禁品数量
     * @return
     */
    @GetMapping("/getCount")
    public ApiResponse getCount(){
        List<Map<String,Object>> res = largeScreenService.getCount();
        return Result.success(res);
    }

    /**
     * 获取给定省内的各市违禁品数量
     * @param areaName
     * @return
     */
    @GetMapping("/getCountByProvince")
    public ApiResponse getCountByProvince(@RequestParam(value = "areaName",required = false) String areaName){
        List<Map<String,Object>> res = largeScreenService.getCountByProvince(areaName);
        return Result.success(res);
    }

    /**
     * 获取给定市内的各区违禁品数量
     * @param areaName
     * @return
     */
    @GetMapping("/getCountByCity")
    public ApiResponse getCountByCity(@RequestParam(value = "areaName",required = false) String areaName){
        List<Map<String,Object>> res = largeScreenService.getCountByCity(areaName);
        return Result.success(res);
    }

    /**
     * 按照给定的日返回给定时间内的违禁品数量
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getDangerCountByTime")
    public ApiResponse getDangerCountByTime(@RequestParam(value = "machineGrade",required = false) String machineGrade,
                                            @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                            @RequestParam(value = "startTime",required = true) String startTime,
                                            @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始按照给定的日，周，月返回给定时间内的违禁品数量");
        List<Map<String, Object>> res = largeScreenService.getDangerCountByTime(machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 整合接口
     * 左下角按地区本日，本周，本月识别的违禁品数
     * @param machineGrade
     * @param machineBrand
     * @return
     */
    @GetMapping("/getTotalDangerCount")
    public ApiResponse getTotalDangerCount(@RequestParam(value = "machineGrade",required = false) String machineGrade,
                                             @RequestParam(value = "machineBrand",required = false) String machineBrand){
        Map<String, Object> res = largeScreenService.getTotalDangerCount(machineGrade,machineBrand);
        return Result.success(res);
    }

    /**
     * 查询按照指定的时间下违禁品的数量排行榜
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getDangerRank")
    public ApiResponse getDangerRank(@RequestParam(value = "machineGrade",required = false) String machineGrade,
                                     @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                     @RequestParam(value = "startTime",required = true) String startTime,
                                     @RequestParam(value = "endTime",required = true) String endTime,
                                     @RequestParam(value = "areaName",required = false) String areaName){
        List<String[]> res = largeScreenService.getDangerRank(machineGrade,machineBrand,startTime,endTime,areaName);
        return Result.success(res);
    }

    /**
     * 大屏地图，获取二级地区的目录
     * @return
     */
    @GetMapping("/getTotal")
    public ApiResponse getTotal(){
        List<Map<String, Object>> res  = new ArrayList<>();
        res.addAll(largeScreenService.getCountByCity(""));
        res.addAll(largeScreenService.getCountByProvince(""));
        List<Map<String, Object>> collect = res.stream().distinct().collect(Collectors.toList());
        return Result.success(collect);
    }

    /**
     * 根据条件获取信息的统计数（统计曲线图使用）
     * @param areaName
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getCountStatistics")
    public ApiResponse getCountStatistics(@RequestParam(value = "areaName",required = true) String areaName,
                                          @RequestParam(value = "machineGrade",required = false) String machineGrade,
                                          @RequestParam(value = "machineBrand",required = false) String machineBrand,
                                          @RequestParam(value = "startTime",required = true) String startTime,
                                          @RequestParam(value = "endTime",required = true) String endTime){
        LogUtils.info("开始根据条件获取信息的统计数");
        List<Map<String, Object>> res = largeScreenService.getCountStatistics(areaName,machineGrade,machineBrand,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 今日过件总量
     * @param areaName
     * @return
     */
    @GetMapping("/getTodayCount")
    public ApiResponse getTodayCount(@RequestParam(value = "areaName",required = false) String areaName){
        Integer res = largeScreenService.getTodayCount(areaName);
        return Result.success(res);
    }

    /**
     * 今日违禁品数量
     * @param areaName
     * @return
     */
    @GetMapping("/getDangerCount")
    public ApiResponse getDangerCount(@RequestParam(value = "areaName",required = false) String areaName){
        Integer res = largeScreenService.getDangerCount(areaName);
        return Result.success(res);
    }

    /**
     * 整合接口
     * 左上角今日过件总量和今日违禁品数量
     * @param areaName
     * @return
     */
    @GetMapping("/todayCount")
    public ApiResponse todayCount(@RequestParam(value = "areaName",required = false) String areaName){
        Map<String, Object> res = largeScreenService.todayCount(areaName);
        return Result.success(res);
    }


    /**
     * 违禁品分类的饼状图
     * @param areaName
     * @return
     */
    @GetMapping("/getDangerByType")
    public ApiResponse getDangerByType(@RequestParam(value = "areaName",required = false) String areaName){
        List<Map<String, Object>> res = largeScreenService.getDangerByType(areaName);
        return Result.success(res);
    }

    /**
     * 折线图，数据统计图，给出首次案件数和总过包数
     * @param areaName
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getTotalByCentre")
    public ApiResponse getTotalByCentre(@RequestParam(value = "areaName",required = false) String areaName,
                           @RequestParam(value = "startTime",required = true) String startTime,
                           @RequestParam(value = "endTime",required = true) String endTime){

        List<Map<String, Object>> res = largeScreenService.getTotalByCentre(areaName,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 过包总数，根据前端传来的值给出结果
     * @param areaName
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getAllCount")
    public ApiResponse getAllCount(@RequestParam(value = "areaName",required = false) String areaName,
                                    @RequestParam(value = "startTime",required = true) String startTime,
                                   @RequestParam(value = "endTime",required = true) String endTime){
        Integer res = largeScreenService.getAllCount(areaName,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 首次安检数，根据前端传来的值给出结果
     * @param areaName
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/DangerCount")
    public ApiResponse DangerCount(@RequestParam(value = "areaName",required = false) String areaName,
                                   @RequestParam(value = "startTime",required = true) String startTime,
                                   @RequestParam(value = "endTime",required = true) String endTime){
        Integer res = largeScreenService.DangerCount(areaName,startTime,endTime);
        return Result.success(res);
    }

    /**
     * 整合接口
     * 获取左下角今日，本周，本月的总过包数和首次案件违禁品数
     * @param areaName
     * @return
     */
    @GetMapping("/totalCount")
    public ApiResponse totalCount(@RequestParam(value = "areaName",required = false) String areaName){
        Map<String,Object> res = largeScreenService.totalCount(areaName);
        return Result.success(res);
    }

    /**
     * 获取各地区的违禁品数量
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("/getDangerArea")
    public ApiResponse getDangerArea(@RequestParam(value = "startTime",required = true) String startTime,
                                     @RequestParam(value = "endTime",required = true) String endTime,
                                     @RequestParam(value = "areaName",required = false) String areaName){
        List<Map<String,Object>> res = largeScreenService.getDangerArea(startTime,endTime,areaName);
        return Result.success(res);
    }

}
