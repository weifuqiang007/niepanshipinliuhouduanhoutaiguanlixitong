package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.common.utils.logs.LogUtils;
import com.niepan.modules.zhinenganjianback.common.UdpServer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liuchenyu
 * @date: 2023/5/16
 */
@RestController
@RequestMapping("/test")
public class test {

    @GetMapping("/test")
    public void test(){
        UdpServer.getInstance();

        UdpServer.returnMsg(UdpServer.leftBytes);

        UdpServer.returnMsg(UdpServer.backBytes);

        LogUtils.info("摆轮已经摆动");
    }
}
