//package com.niepan.modules.zhinenganjianback.controller;
//
//
//import com.niepan.common.utils.R.ApiResponse;
//import com.niepan.common.utils.R.Result;
//import com.niepan.modules.zhinenganjianback.VO.JsonObjectVO;
//import com.niepan.modules.zhinenganjianback.VO.ThresholdVO;
//import com.niepan.modules.zhinenganjianback.service.DeviceManagerService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.annotation.PostConstruct;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/api/deviceManager")
//public class DeviceManagerController{
//
//    @Autowired
//    private DeviceManagerService deviceManagerService;
//
//    //在项目启动的时候就开始初始化
//    @PostConstruct
//    @GetMapping("/init")
//    public void init(){
//        deviceManagerService.init();
//    }
//
//    //0 查看设备状态 View device status
//    @GetMapping("/viewDeviceStatus")
//    public ApiResponse viewDeviceStatus(){
//        deviceManagerService.viewDeviceStatus();
//        return Result.success();
//    }
//
//    //1 查询阈值
//    @GetMapping("/queryThreshold")
//    public ApiResponse queryThreshold(){
//        long t1 = System.currentTimeMillis();
//        List<Map<String,Object>> res = deviceManagerService.queryThreshold();
//        long t2 = System.currentTimeMillis();
//        System.out.println(t2 - t1);
//        return Result.success(res);
//    }
//
//    //功能可实现，但是时间不稳定
//    //2 设置全部物品阈值
//    @PostMapping("/setThreshold")
//    public ApiResponse setThreshold(@RequestBody List<ThresholdVO> thresholdVOList){
//        long t1 = System.currentTimeMillis();
//        deviceManagerService.setThreshold(thresholdVOList);
//        long t2 = System.currentTimeMillis();
//        System.out.println(t2 - t1);
//        return Result.success();
//    }
//
//    //3 推送图片
//    @PostMapping("/pushPictures")
//    public ApiResponse pushPictures(@RequestBody MultipartFile file){
//        long t1 = System.currentTimeMillis();
//        deviceManagerService.pushPictures(file);
//        long t2 = System.currentTimeMillis();
//        System.out.println("pushPicture + " + (t2 - t1));
//        return Result.success();
//    }
//
//    //3 推送图片（参数为base64）
//    @PostMapping("/pushPicturesByBase64")
//    public ApiResponse pushPicturesByBase64(@RequestBody JsonObjectVO jsonObjectVO){
//        long t1 = System.currentTimeMillis();
//        deviceManagerService.pushPicture(jsonObjectVO);
//        long t2 = System.currentTimeMillis();
//        System.out.println("pushPicture + " + (t2 - t1));
//        return Result.success();
//    }
//
//    //4 退出
//    @PostMapping("/exit")
//    public ApiResponse exit(){
//        deviceManagerService.exit();
//        return Result.success();
//    }
//
//    //保存图片  文件流
//    @PostMapping("/savePicture")
//    public ApiResponse savePicture(@RequestBody MultipartFile file){
//        deviceManagerService.savePicture(file);
//        return Result.success();
//    }
//
//    //保存图片  json对象(base64)
//    @PostMapping("/savePictureByBase64")
//    public ApiResponse savePictureByBase64(@RequestBody JsonObjectVO jsonObjectVO){
//        Integer integer = deviceManagerService.savePicture(jsonObjectVO);
//        if (integer > 0){
//            return Result.success(integer);
//        }
//        return Result.error();
//    }
//}