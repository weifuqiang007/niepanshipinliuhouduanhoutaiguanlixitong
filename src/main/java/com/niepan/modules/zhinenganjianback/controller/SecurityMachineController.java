//package com.niepan.modules.zhinenganjianback.controller;
//
//import com.niepan.common.utils.R.ApiResponse;
//import com.niepan.common.utils.R.Result;
//import com.niepan.common.utils.heart.HeartInfo;
//import com.niepan.common.utils.json.JsonToFile;
//import com.niepan.common.utils.logs.LogUtils;
//import com.niepan.modules.zhinenganjianback.dto.GetHeartBeatDTO;
//import com.niepan.modules.zhinenganjianback.dto.GetHeartInfoDto;
//import com.niepan.modules.zhinenganjianback.dto.GetImgDTO;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
//import com.niepan.modules.zhinenganjianback.service.SecurityMachineService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.PostConstruct;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Timer;
//
//
///**
// * @author weifuqiang
// * @create_time 2022/11/26
// */
//
//@RestController
//@RequestMapping("/bjhy/api/securitydevice")
//public class SecurityMachineController {
//
//    @Value("${file.picPath}")
//    private String picPath;
//
//    // 全局变量 service的全局变量
//    @Autowired
//    private SecurityMachineService securityMachineService;
//
//    // 实现接受图片的请求
//    @RequestMapping(value = "/img_send", method = RequestMethod.POST)
//    public ApiResponse getImgInfo(@RequestBody GetImgDTO getImgDTO){
//
//        System.out.println(getImgDTO.getDevice_sn());
//        System.out.println(getImgDTO.getTime());
//        System.out.println(getImgDTO.getLine_code());
//
//        String picName = getImgDTO.getDevice_sn()+"@&"+getImgDTO.getLine_code()+"@&"+getImgDTO.getTime() + ".jpg";
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//        String datePath = sdf.format(new Date());//20230215
//        //存放图片的路径
//        String filePath = picPath + getImgDTO.getDevice_sn() + getImgDTO.getLine_code() + "/" + datePath + "/";
//
//        //图片存储到本地
//        JsonToFile.stringToJpg(getImgDTO.getImg_base64_side(),filePath+picName);
//
//        //日志记录
//        LogUtils.info("photo path is " + filePath + picName);
//        LogUtils.info("photo info is " + getImgDTO.getTime());
//
//        //将要插入数据库的数据封装到对象中
//        SecurityMachinePicture securityMachinePicture = new SecurityMachinePicture();
//        securityMachinePicture.setName(picName);
//        securityMachinePicture.setDevice_sn(getImgDTO.getDevice_sn());
//        securityMachinePicture.setLine_code(getImgDTO.getLine_code());
//        securityMachinePicture.setTime(getImgDTO.getTime().toString());
//
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
//        String date = df.format(new Date());//2023-02-17 13:49:08
//        securityMachinePicture.setCurrentTime(date);
//
//        //数据库插入 并推送给大华设备
//        Boolean res = securityMachineService.getImgInfo(securityMachinePicture,getImgDTO.getImg_base64_side());
//        if (res != null && res){
//            LogUtils.info("图片保存成功");
//            return Result.success("图片保存成功");
//        }
//        LogUtils.error("保存图片失败" + picName);
//        return Result.error("保存图片失败");
//    }
//
//
//    // 安检机状态和故障信息上报
//    @RequestMapping(value = "/status_send", method = RequestMethod.POST)
//    public ApiResponse getStatus(@RequestBody GetHeartBeatDTO getHeartBeatDTO){
//        System.out.println(getHeartBeatDTO.getDevice_sn());
//        System.out.println(getHeartBeatDTO.getLine_code());
//        System.out.println(getHeartBeatDTO.getStatus_code());
//        System.out.println(getHeartBeatDTO.getFault_level());
//        System.out.println(getHeartBeatDTO.getTime());
//        System.out.println(getHeartBeatDTO.getStatus_desc());
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        if (getHeartBeatDTO.getFault_level().equals("0")){
//            LogUtils.info("安检机状态正常");
//            LogUtils.info(formatter.format(new Date()) +"@#"+ getHeartBeatDTO.getDevice_sn() +"@#"+
//                    getHeartBeatDTO.getFault_level() +"@#" +getHeartBeatDTO.getStatus_desc());
//        }
//        else{
//            LogUtils.error("安检机状态异常");
//            LogUtils.error(formatter.format(new Date()) +"@#"+ getHeartBeatDTO.getDevice_sn() +"@#"+
//                    getHeartBeatDTO.getFault_level() +"@#" +getHeartBeatDTO.getStatus_desc());
//        }
//
//        SecurityMachineStatus securityMachineStatus = new SecurityMachineStatus();
//        securityMachineStatus.setDevice_sn(getHeartBeatDTO.getDevice_sn());
//        securityMachineStatus.setLine_code(getHeartBeatDTO.getLine_code());
//        securityMachineStatus.setStatus_code(getHeartBeatDTO.getStatus_code());
//        securityMachineStatus.setStatus_desc(getHeartBeatDTO.getStatus_desc());
//        securityMachineStatus.setFault_level(getHeartBeatDTO.getFault_level());
//        securityMachineStatus.setTime(getHeartBeatDTO.getTime());
//        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
//        String date = df.format(new Date());//20230215
//        securityMachineStatus.setMsgtime(date);
//
//        String status = securityMachineService.SecurityMachineStatus(securityMachineStatus);
//        System.out.println("status"+status);
//
//        return Result.success(status);
//    }
//
//    SimpleDateFormat num = new SimpleDateFormat("HH:mm:ss");
//    String formatOld = num.format(new Date());
//
//    // 安检机心跳
//    @RequestMapping(value = "/heart", method = RequestMethod.POST)
//    public ApiResponse getHeart(@RequestBody GetHeartInfoDto getHeartInfoDto){
//
//        System.out.println("Device_sn is : "+getHeartInfoDto.getDevice_sn());
//        System.out.println("Line_code is : "+getHeartInfoDto.getLine_code());
//        System.out.println("Status_code is : "+getHeartInfoDto.getStatus_code());
//        Date date = new Date();
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(formatter.format(date));
//
//        if (getHeartInfoDto.getStatus_code().equals("4")){
//            LogUtils.error("heart beat time is :"+formatter.format(date) + "Device_sn is :"+getHeartInfoDto.getDevice_sn() +
//                    "Line_code is :"+ getHeartInfoDto.getLine_code() + "Status_code() is :" + getHeartInfoDto.getStatus_code());
//
//        }else {
//            LogUtils.info("heart beat time is :"+formatter.format(date) + "Device_sn is :"+getHeartInfoDto.getDevice_sn() +
//                    "Line_code is :"+ getHeartInfoDto.getLine_code() + "Status_code() is :" + getHeartInfoDto.getStatus_code());
//        }
//        try {
//
//            String format = num.format(new Date());
//
//            Date dateOld=num.parse(formatOld);
//            Date date2=num.parse(format);
//
//            if (date2.getTime() - dateOld.getTime() > 10000){
//                securityMachineService.getHeart(getHeartInfoDto);
//                formatOld = format;
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return Result.success();
//    }
//
//}