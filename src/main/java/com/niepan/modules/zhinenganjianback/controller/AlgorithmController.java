package com.niepan.modules.zhinenganjianback.controller;

import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.modules.zhinenganjianback.model.AlgorithmVo;
import com.niepan.modules.zhinenganjianback.service.AlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/Algorithm")
public class AlgorithmController {

    @Autowired
    private AlgorithmService algorithmService;

    @GetMapping("/list")
    public ApiResponse list(){
        List<Map<String,Object>> list = algorithmService.list();
        return Result.success(list);
    }

    @PostMapping("/addAlgorithm")
    public ApiResponse addAlgorithm(@RequestBody AlgorithmVo algorithmVo){
        Integer res = algorithmService.addAlgorithm(algorithmVo);
        return Result.success(res);
    }

    @GetMapping("/deviceDetail")
    public ApiResponse deviceDetail(){
        List<Map<String,Object>> res = algorithmService.deviceDetail();
        return Result.success(res);
    }

}
