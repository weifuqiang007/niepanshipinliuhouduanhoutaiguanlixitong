//package com.niepan.modules.zhinenganjianback.controller;
//
//
//import com.niepan.common.utils.R.ApiResponse;
//import com.niepan.common.utils.R.Result;
//import com.niepan.common.utils.json.JsonToFile;
//import com.niepan.common.utils.logs.LogUtils;
//import com.niepan.modules.zhinenganjianback.VO.AtxImgVO;
//import com.niepan.modules.zhinenganjianback.VO.AtxStatusVO;
//import com.niepan.modules.zhinenganjianback.dto.GetHeartInfoDto;
//import com.niepan.modules.zhinenganjianback.enums.AtxStatusEnum;
//import com.niepan.modules.zhinenganjianback.model.AtxHeartBeat;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
//import com.niepan.modules.zhinenganjianback.service.AtxSecurityMachineService;
//import com.niepan.modules.zhinenganjianback.service.SecurityMachineService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.sql.Timestamp;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * 安天下安检机接口
// */
//@RestController
//@RequestMapping("/atx/api/securitydevice")
//public class AtxSecurityMachineController {
//
//    @Value("${file.picPath}")
//    private String picPath;
//
//    @Autowired
//    private SecurityMachineService securityMachineService;
//
//    @Autowired
//    private AtxSecurityMachineService atxSecurityMachineService;
//
//    // 实现接受图片的请求
//    @RequestMapping(value = "/img_send", method = RequestMethod.POST)
//    public ApiResponse img_send(@RequestBody AtxImgVO atxImgVO){
//
//        String substring = atxImgVO.getChecktime().substring(0, atxImgVO.getChecktime().lastIndexOf(" "));
//        Timestamp timestamp = Timestamp.valueOf(substring);
//        long time = timestamp.getTime();
//        String picName = atxImgVO.getDeviceid() + "@#" + time + ".jpg";
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//        String datePath = sdf.format(new Date());//20230215
//        //存放图片的路径
//        String filePath = picPath + atxImgVO.getDeviceid() + "/" + datePath + "/";
//
//        //图片存储到本地
//        JsonToFile.stringToJpg(atxImgVO.getTopimagedata(),filePath+picName);
//
//        //日志记录
//        LogUtils.info("photo path is " + filePath + picName);
//        LogUtils.info("photo info is " + atxImgVO.getChecktime());
//
//        //将要插入数据库的数据封装到对象中
//        SecurityMachinePicture securityMachinePicture = new SecurityMachinePicture();
//        securityMachinePicture.setName(picName);
//        securityMachinePicture.setDevice_sn(atxImgVO.getDeviceid());
//        securityMachinePicture.setLine_code("1");//安天下安检机没有信道，默认为1
//        securityMachinePicture.setTime(atxImgVO.getChecktime());
//
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
//        String date = df.format(new Date());//2023-02-17 13:49:08
//        securityMachinePicture.setCurrentTime(date);
//
//        //数据库插入 并推送给大华设备
//        Boolean res = atxSecurityMachineService.getImgInfo(securityMachinePicture,atxImgVO.getTopimagedata());
//        if (res){
//            LogUtils.info("图片保存成功");
//            return Result.success("图片保存成功");
//        }
//        LogUtils.error("保存图片失败" + picName);
//        return Result.error("保存图片失败");
//    }
//
//    // 安检机状态和故障信息上报
//    @RequestMapping(value = "/status_send", method = RequestMethod.POST)
//    public ApiResponse getStatus(@RequestBody AtxStatusVO atxStatusVO){
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        if (atxStatusVO.getErrortype().equals("0")){
//            LogUtils.info("安检机状态正常");
//            LogUtils.info(formatter.format(new Date()) + "@#" + atxStatusVO.getDeviceid() +"@#"+
//                    atxStatusVO.getErrortype() + "@#" + AtxStatusEnum.getValue(atxStatusVO.getErrortype()));
//        }
//        else{
//            LogUtils.error("安检机状态异常");
//            LogUtils.error(formatter.format(new Date()) + "@#" + atxStatusVO.getDeviceid() +"@#"+
//                    atxStatusVO.getErrortype() + "@#" + AtxStatusEnum.getValue(atxStatusVO.getErrortype()));
//        }
//
//        SecurityMachineStatus securityMachineStatus = new SecurityMachineStatus();
//        securityMachineStatus.setDevice_sn(atxStatusVO.getDeviceid());
//        securityMachineStatus.setLine_code("1");//安天下安检机没有信道，默认为1
//        securityMachineStatus.setStatus_code(atxStatusVO.getErrortype().toString());
//        securityMachineStatus.setStatus_desc(AtxStatusEnum.getValue(atxStatusVO.getErrortype()));
//        securityMachineStatus.setFault_level(atxStatusVO.getDevicestatus().toString());
//        //传来的时间是yyyy-MM-dd HH:mm:ss SSS 将其转化为时间戳
//        String substring = atxStatusVO.getErrortime().substring(0, atxStatusVO.getErrortime().lastIndexOf(" "));
//        Timestamp timestamp = Timestamp.valueOf(substring);
//        long time = timestamp.getTime();
//        securityMachineStatus.setTime(time);
//        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
//        String date = df.format(new Date());//20230215
//        securityMachineStatus.setMsgtime(date);
//
//        String status = atxSecurityMachineService.SecurityMachineStatus(securityMachineStatus);
//
//        return Result.success(status);
//    }
//
//    SimpleDateFormat num = new SimpleDateFormat("HH:mm:ss");
//    String formatOld = num.format(new Date());
//
//    // 安检机心跳
//    @RequestMapping(value = "/heart", method = RequestMethod.POST)
//    public ApiResponse getHeart(@RequestBody AtxHeartBeat atxHeartBeat){
//
//        System.out.println(atxHeartBeat.getDatatime());
//        System.out.println(atxHeartBeat.getProtocolversion());
//        Date date = new Date();
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(formatter.format(date));
//
//        LogUtils.info("heart beat time is :"+formatter.format(date) + "deviceId is :"+atxHeartBeat.getDeviceid() +
//                "ProtocolVersion is :"+ atxHeartBeat.getProtocolversion());
//
//        try {
//
//            String format = num.format(new Date());
//
//            Date dateOld=num.parse(formatOld);
//            Date date2=num.parse(format);
//
//            if (date2.getTime() - dateOld.getTime() > 10000){
//                GetHeartInfoDto getHeartInfoDto = new GetHeartInfoDto();
//                getHeartInfoDto.setDevice_sn(atxHeartBeat.getDeviceid());
//                securityMachineService.getHeart(getHeartInfoDto);
//                formatOld = format;
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        return Result.success();
//    }
//}
