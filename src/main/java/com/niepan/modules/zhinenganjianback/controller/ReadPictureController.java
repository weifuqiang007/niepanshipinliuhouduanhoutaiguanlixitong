//package com.niepan.modules.zhinenganjianback.controller;
//
//import com.niepan.modules.zhinenganjianback.dto.GetImgDTO;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import sun.misc.BASE64Encoder;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// *  批量读取某个文件夹下的图片
// */
//@RestController
//@RequestMapping("/picture")
//public class ReadPictureController {
//
//    @Autowired
//    SecurityMachineController securityMachineController;
//
//    /**
//     * 读取某路径下的所有图片并推送给大华机器
//     * @throws Exception
//     */
//    @PostMapping("/test")
//    public void test() throws Exception {
//        List<String> fileNames = this.getFileNames("D:\\pic");
//        for (String fileName : fileNames) {
//            GetImgDTO getImgDTO = new GetImgDTO();
//            getImgDTO.setDevice_sn("9E07FB1GAJ00002");
//            getImgDTO.setLine_code("1");
//            getImgDTO.setImg_base64_side(fileName);
//            getImgDTO.setTime(System.currentTimeMillis());
//            securityMachineController.getImgInfo(getImgDTO);
//            Thread.sleep(300);
//        }
//    }
//
//    public List<String> getFileNames(String path) throws Exception {
//        File file = new File(path);
//        if (!file.exists()) {
//            return null;
//        }
//        return getFileNames(file);
//    }
//
//    public List<String> getFileNames(File file) throws Exception {
//        List<String> list = new ArrayList<>();
//        File[] files = file.listFiles();
//        for (File f : files) {
//            FileInputStream inputFile = new FileInputStream(f);
//            byte[] buffer = new byte[(int)f.length()];
//            inputFile.read(buffer);
//            inputFile.close();
//            list.add(new BASE64Encoder().encode(buffer));
//        }
//        return list;
//    }
//}
