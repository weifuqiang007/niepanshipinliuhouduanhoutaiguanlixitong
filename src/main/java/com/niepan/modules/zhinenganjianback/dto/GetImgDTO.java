package com.niepan.modules.zhinenganjianback.dto;

import lombok.Data;

/**
 * @author weifuqiang
 * @create_time 2022/11/26
 * 普通的对象
 */

@Data
public class GetImgDTO {
    /**
     * 安检机设备序列号,需提供可配置入口
     *
     */
    String device_sn;

    /**
     * 生产线号(1-18) ,需提供可配置入口
     */
    String line_code ;

    /**
     * 侧视角 base64 图片,文件大小<2M（不传头信息）
     */
    String img_base64_side;


    /**
     * 数据采集时间戳，ms
     */
    Long time;

    /**
     *
     */

    Integer id;


}
