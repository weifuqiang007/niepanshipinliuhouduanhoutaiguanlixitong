package com.niepan.modules.zhinenganjianback.enums;

public enum AtxStatusEnum {
    NOT_ERROR(0,"无"),
    NOT_CONNECTED(6100,"未能与主控制板建立连接"),
    EMERGENCY_STOP(6102,"急停开关触发"),
    OUTER_COVER_PLATE_INCHING(6104,"外罩板微动开关触发"),
    COLLECTION_SYSTEM_FAULT(6105,"采集系统故障");

    AtxStatusEnum(Integer status,String message) {
        this.status = status;

        this.message = message;
    }

    private Integer status;

    private String message;

    public Integer status() {
        return this.status;
    }

    public String message() {
        return this.message;
    }

    public static String getValue(Integer status) {
        AtxStatusEnum[] atxStatusEnums = values();
        for (AtxStatusEnum atxStatusEnum : atxStatusEnums) {
            if (atxStatusEnum.status() == status) {
                return atxStatusEnum.message();
            }
        }
        return null;
    }

}
