package com.niepan.modules.zhinenganjianback.dao;

import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;

public interface DhSecurityMachineDao {
    Integer securityMachineStatus(SecurityMachineStatus securityMachineStatus);
}
