package com.niepan.modules.zhinenganjianback.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.niepan.modules.zhinenganjianback.dto.AreaDwscode;
import org.apache.ibatis.annotations.Param;

/**
* @author zq4568
* @description 针对表【area_dwscode】的数据库操作Mapper
* @createDate 2023-08-16 17:12:33
* @Entity com.niepan.entity.AreaDwscode
*/
public interface AreaDwscodeMapper extends BaseMapper<AreaDwscode> {

    String selectByDwsCode(@Param("dwsCode") String dwsCode);
}
