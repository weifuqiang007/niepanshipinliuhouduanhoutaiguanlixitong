package com.niepan.modules.zhinenganjianback.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.niepan.modules.zhinenganjianback.dto.AreaMap;

/**
* @author zq4568
* @description 针对表【area_map】的数据库操作Mapper
* @createDate 2023-08-18 15:19:28
* @Entity com.niepan.entity.AreaMap
*/
public interface AreaMapMapper extends BaseMapper<AreaMap> {

}
