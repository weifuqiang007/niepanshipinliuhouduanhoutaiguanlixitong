package com.niepan.modules.zhinenganjianback.dao;

import com.niepan.modules.zhinenganjianback.model.HeartBeat;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author weifuqiang
 * @create_time 2022/12/19
 */
@Repository
public interface HeartBeatDao {
    Integer Add(HeartBeat heartBeat);

    List<Map<String, Object>> getInfo();

    Map<String, Object> getSnAndDwsCode(@Param("ip") String ip);
}
