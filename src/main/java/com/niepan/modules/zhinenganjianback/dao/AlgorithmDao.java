package com.niepan.modules.zhinenganjianback.dao;

import com.niepan.modules.zhinenganjianback.model.AlgorithmVo;

import java.util.List;
import java.util.Map;

public interface AlgorithmDao {
    List<Map<String, Object>> list();

    Integer addAlgorithm(AlgorithmVo algorithmVo);

    List<Map<String, Object>> deviceDetail();
}
