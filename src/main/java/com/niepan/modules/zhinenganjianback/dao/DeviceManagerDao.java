package com.niepan.modules.zhinenganjianback.dao;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DeviceManagerDao {

    void insertImage(@Param("emDangerGrade") int emDangerGrade,
                     @Param("emObjType") int emObjType,
                     @Param("emObjName") String emObjName,
                     @Param("nSimilarity") int nSimilarity,
                     @Param("stuBoundingBoxs") String stuBoundingBoxs,
                     @Param("id") String id,
                     @Param("areaCode") String areaCode);

    void updateThreshold(@Param("value") int value,
                         @Param("note") String note,
                         @Param("threshold") Integer threshold,
                         @Param("personName") String personName,
                         @Param("deviceIP") String deviceIP,
                         @Param("enable") Integer enable);

    @MapKey("id")
    List<Map<String,Object>> getObjectByType(@Param("value") int value);

    void insertThresholdForOld(@Param("emObjType") int emObjType,
                               @Param("itemName") String itemName,
                               @Param("threshole") int threshole,
                               @Param("personName") String personName,
                               @Param("deviceIP") String deviceIP,
                               @Param("enable") Integer enable);


    Integer savePicture(@Param("uuid") String uuid,
                        @Param("name") String name,
                        @Param("device_sn") String device_sn,
                        @Param("line_code") String line_code,
                        @Param("time") Long time,
                        @Param("currentTime")String currentTime);

    Map<String, Object> getAllDevice(@Param("device_sn") String device_sn);

    List<Map<String, Object>> getDevice(@Param("ip") String ip);

    String getDeviceIp(@Param("valueOf") String valueOf);

    String getDWSNcode(@Param("snCode") String snCode);

    String getCurrentTime(@Param("id") String id);

    Map<String,Object> getPicName(@Param("picture_id") String picture_id);

    void updateHttp(@Param("id") String id);

    void updateFTP(@Param("imageId") String imageId);

    Map<String,Object> getRequestURL();

    Map<String, Object> getSwing();

//    String getDWSNcode(@Param("deviceIp") String deviceIp);
}
