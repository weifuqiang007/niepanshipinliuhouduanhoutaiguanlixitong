package com.niepan.modules.zhinenganjianback.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.niepan.modules.zhinenganjianback.model.ImageIdentificationRecords;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageIdentificationRepository extends BaseMapper<ImageIdentificationRecords> {
}
