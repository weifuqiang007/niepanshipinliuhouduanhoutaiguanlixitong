package com.niepan.modules.zhinenganjianback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.niepan.modules.zhinenganjianback.dao.AreaCountMapper;
import com.niepan.modules.zhinenganjianback.dto.AreaCount;
import com.niepan.modules.zhinenganjianback.service.AreaCountService;
import org.springframework.stereotype.Service;

/**
* @author zq4568
* @description 针对表【area_count】的数据库操作Service实现
* @createDate 2023-08-16 17:12:33
*/
@Service
public class AreaCountServiceImpl extends ServiceImpl<AreaCountMapper, AreaCount>
    implements AreaCountService {

    @Override
    public int insertData(AreaCount areaCount) {
        int insert = baseMapper.insert(areaCount);
        return insert;
    }

    @Override
    public String dayAndAreaCode(String areaCode) {
        return baseMapper.dayAndAreaCode(areaCode);
    }

    @Override
    public int updateDangerCount(Integer integer) {

        return baseMapper.updateDangerCount(integer);
    }

    @Override
    public int updateNoDangerCount(Integer integer) {
        return baseMapper.updateNoDangerCount(integer);
    }
}