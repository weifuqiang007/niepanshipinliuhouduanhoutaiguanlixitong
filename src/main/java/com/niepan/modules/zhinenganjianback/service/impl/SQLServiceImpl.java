package com.niepan.modules.zhinenganjianback.service.impl;

import com.niepan.modules.zhinenganjianback.dao.SQLDao;
import com.niepan.modules.zhinenganjianback.model.AreaVO;
import com.niepan.modules.zhinenganjianback.model.BalanceWheelVO;
import com.niepan.modules.zhinenganjianback.model.NPDeviceRegister;
import com.niepan.modules.zhinenganjianback.model.RequestUrlVO;
import com.niepan.modules.zhinenganjianback.service.SQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author: liuchenyu
 * @date: 2023/6/25
 */
@Service
public class SQLServiceImpl implements SQLService {

    @Autowired
    private SQLDao sqlDao;

    @Override
    public Integer getTableInformation(String table) {
        Integer res = sqlDao.getTableInformation(table);
        return res;
    }

    @Override
    public List<Map<String,Object>> getDevice() {
        List<Map<String,Object>> res = sqlDao.getDevice();
        return res;
    }

    @Override
    public Integer deleteDevice() {
        Integer res = sqlDao.deleteDevice();
        return res;
    }

    @Override
    public Integer insertDevice(NPDeviceRegister npDeviceRegister) {
        Integer res = sqlDao.insertDevice(npDeviceRegister);
        return res;
    }

    @Override
    public List<Map<String, Object>> getArea() {
        List<Map<String,Object>> res = sqlDao.getArea();
        return res;
    }

    @Override
    public Integer deleteArea() {
        Integer res = sqlDao.deleteArea();
        return res;
    }

    @Override
    public Integer insertArea(AreaVO areaVO) {
        Integer res = sqlDao.insertArea(areaVO);
        return res;
    }

    @Override
    public List<Map<String, Object>> getRequestUrl() {
        List<Map<String,Object>> res = sqlDao.getRequestUrl();
        return res;
    }

    @Override
    public Integer deleteRequestUrl() {
        Integer res = sqlDao.deleteRequestUrl();
        return res;
    }

    @Override
    public Integer insertRequestUrl(RequestUrlVO requestUrlVO) {
        Integer res = sqlDao.insertRequestUrl(requestUrlVO);
        return res;
    }

    @Override
    public List<Map<String, Object>> getBalanceWheel() {
        List<Map<String,Object>> res = sqlDao.getBalanceWheel();
        return res;
    }

    @Override
    public Integer deleteBalanceWheel() {
        Integer res = sqlDao.deleteBalanceWheel();
        return res;
    }

    @Override
    public Integer insertBalanceWheel(BalanceWheelVO balanceWheelVO) {
        Integer res = sqlDao.insertBalanceWheel(balanceWheelVO);
        return res;
    }
}
