//package com.niepan.modules.zhinenganjianback.service.impl;
//
//import com.niepan.common.utils.R.ResultStatusEnum;
//import com.niepan.common.utils.logs.LogUtils;
//import com.niepan.modules.zhinenganjianback.VO.JsonObjectVO;
//import com.niepan.modules.zhinenganjianback.dao.DhSecurityMachineDao;
//import com.niepan.modules.zhinenganjianback.demo.DeviceManage;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
//import com.niepan.modules.zhinenganjianback.service.DhSecurityMachineService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * @author: liuchenyu
// * @date: 2023/5/18
// */
//@Service
//public class DhSecurityMachineServiceImpl implements DhSecurityMachineService {
//
//    @Autowired
//    private DeviceManage deviceManage;
//
//    @Autowired
//    private DhSecurityMachineDao dhSecurityMachineDao;
//
//    @Override
//    public Boolean getImgInfo(SecurityMachinePicture securityMachinePicture, String topImageData) {
//        JsonObjectVO jsonObjectVO = new JsonObjectVO();
//        jsonObjectVO.setDevice_sn(securityMachinePicture.getDevice_sn());
//        jsonObjectVO.setLine_code(securityMachinePicture.getLine_code());
//        jsonObjectVO.setName(securityMachinePicture.getName());
//        //传来的时间是yyyy-MM-dd HH:mm:ss SSS 将其转化为时间戳
//        String substring = securityMachinePicture.getTime().substring(0, securityMachinePicture.getTime().lastIndexOf(" "));
//        Timestamp timestamp = Timestamp.valueOf(substring);
//        long time = timestamp.getTime();
//        jsonObjectVO.setTime(time);
//        jsonObjectVO.setCurrentTime(securityMachinePicture.getCurrentTime());
//        jsonObjectVO.setImg_base64_side(topImageData);
//        Boolean aBoolean = deviceManage.newPushPicture(jsonObjectVO);
//        return aBoolean;
//    }
//
//    @Override
//    public String SecurityMachineStatus(SecurityMachineStatus securityMachineStatus) {
//        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//        String currentTimeFormat = formatter.format(new Date());
//
//        securityMachineStatus.setMsgtime(currentTimeFormat);
//        Integer res = dhSecurityMachineDao.securityMachineStatus(securityMachineStatus);
//
//        if(res == 1 &&  "0".equals(securityMachineStatus.getFault_level())){
//            LogUtils.info("安检机状态正常");
//            LogUtils.info(formatter.format(new Date()) +"@#"+ securityMachineStatus.getDevice_sn() +"@#"+
//                    securityMachineStatus.getFault_level() +"@#" + securityMachineStatus.getStatus_desc());
//            return securityMachineStatus.getFault_level();
//        }else if (res == 0){
//            LogUtils.error("数据库插入异常");
//            LogUtils.error(formatter.format(new Date()) +"@#"+ securityMachineStatus.getDevice_sn() +"@#"+
//                    securityMachineStatus.getFault_level() +"@#" + securityMachineStatus.getStatus_desc());
//            return  ResultStatusEnum.INSERT_ERROR_STATUS.getStatus().toString();
//        }
//        LogUtils.error("安检机状态异常");
//        LogUtils.error(formatter.format(new Date()) +"@#"+ securityMachineStatus.getDevice_sn() +"@#"+
//                securityMachineStatus.getFault_level() +"@#" + securityMachineStatus.getStatus_desc());
//        return securityMachineStatus.getFault_level();
//    }
//}
