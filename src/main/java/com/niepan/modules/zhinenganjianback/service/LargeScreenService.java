package com.niepan.modules.zhinenganjianback.service;

import com.niepan.common.utils.R.ApiResponse;
import com.niepan.modules.zhinenganjianback.VO.LargeScreenDataVo;
import com.niepan.modules.zhinenganjianback.dto.AreaCount;
import com.niepan.modules.zhinenganjianback.dto.AreaDwscode;

import java.util.List;
import java.util.Map;

/**
 * @author: liuchenyu
 * @date: 2023/3/27
 */
public interface LargeScreenService {
    /**
     * 获取全国各省的违禁品数量
     * @return
     */
    List<Map<String, Object>> getCount();

    /**
     * 获取给定省内的各市违禁品数量
     * @param areaName
     * @return
     */
    List<Map<String, Object>> getCountByProvince(String areaName);

    /**
     * 获取给定市内的各区违禁品数量
     * @param areaName
     * @return
     */
    List<Map<String, Object>> getCountByCity(String areaName);

    /**
     * 按照给定的日，周，月返回给定时间内的违禁品数量
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getDangerCountByTime(String machineGrade, String machineBrand, String startTime, String endTime);

    /**
     * 查询按照指定的时间下违禁品的数量排行榜
     * @param machineGrade
     * @param machineBrand
     * @param startTime
     * @param endTime
     * @return
     */
    List<String[]> getDangerRank(String machineGrade, String machineBrand, String startTime, String endTime,String areaName);

    List<Map<String, Object>> getTotal();

    List<Map<String, Object>> getCountStatistics(String areaName, String machineGrade, String machineBrand, String startTime, String endTime);

    Integer getTodayCount(String areaName);

    Integer getDangerCount(String areaName);

    List<Map<String, Object>> getDangerByType(String areaName);

    List<Map<String, Object>> getTotalByCentre(String areaName, String startTime, String endTime);

    Integer getAllCount(String areaName, String startTime, String endTime);

    Integer DangerCount(String areaName, String startTime, String endTime);

    List<Map<String, Object>> getDangerArea(String startTime, String endTime,String areaName);

    Map<String, Object> getTotalDangerCount(String machineGrade, String machineBrand);

    Map<String,Object> totalCount(String areaName);

    Map<String, Object> todayCount(String areaName);

    ApiResponse receiveInfo(LargeScreenDataVo largeScreenDataVo);

    String dayAndAreaCode(AreaCount areaCount, AreaDwscode areaDwscode, String today);
}
