package com.niepan.modules.zhinenganjianback.service;


import com.niepan.modules.zhinenganjianback.model.PhotoInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author weifuqiang
 * @create_time 2022/12/18
 */

public interface PhotoInfoService {

    Boolean insertPhotoInfo(PhotoInfo photoInfo);

}
