package com.niepan.modules.zhinenganjianback.service;

import com.niepan.modules.zhinenganjianback.model.NPDeviceRegister;

import java.util.ArrayList;

/**
 * @author weifuqiang
 * @date 2023/3/8
 *
 * 关于服务器注册地址、ip等相关信息的接口
 */


public interface NPDeviceService {
    /**
     * 列出当前所有服务器信息
     * @param status 状态（0 不可用 1 可用） 默认为 1
     * @return 包含服务器信息的列表
     */
    ArrayList<NPDeviceRegister> ListAllDevice(Integer status);
}
