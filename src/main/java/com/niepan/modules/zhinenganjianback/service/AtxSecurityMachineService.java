//package com.niepan.modules.zhinenganjianback.service;
//
//import com.niepan.modules.zhinenganjianback.model.SecurityMachinePicture;
//import com.niepan.modules.zhinenganjianback.model.SecurityMachineStatus;
//
//public interface AtxSecurityMachineService {
//    /**
//     * 安检机把接受图片的信息放到数据库中记录
//     * @param securityMachinePicture
//     */
//    Boolean getImgInfo(SecurityMachinePicture securityMachinePicture, String topImageData);
//
//    /**
//     * 安检机状态和故障信息上报
//     * @param securityMachineStatus 安检机状态对象
//     * @return fault_level 故障级别，0 下线 1上线 2 故障
//     */
//    String SecurityMachineStatus(SecurityMachineStatus securityMachineStatus);
//}
