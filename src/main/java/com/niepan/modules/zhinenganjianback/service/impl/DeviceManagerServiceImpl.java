//package com.niepan.modules.zhinenganjianback.service.impl;
//
//import com.niepan.common.utils.logs.LogUtils;
//import com.niepan.modules.zhinenganjianback.VO.JsonObjectVO;
//import com.niepan.modules.zhinenganjianback.VO.ThresholdVO;
//import com.niepan.modules.zhinenganjianback.dao.DeviceManagerDao;
//import com.niepan.modules.zhinenganjianback.dao.NPDeviceDao;
//import com.niepan.modules.zhinenganjianback.demo.DeviceClient;
//import com.niepan.modules.zhinenganjianback.demo.DeviceManage;
//import com.niepan.modules.zhinenganjianback.lib.enumeration.EM_INSIDE_OBJECT_TYPE;
//import com.niepan.modules.zhinenganjianback.service.DeviceManagerService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//import org.springframework.web.multipart.MultipartFile;
//import sun.misc.BASE64Decoder;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//@Service
//public class DeviceManagerServiceImpl implements DeviceManagerService {
//
//    @Autowired
//    private DeviceManagerDao deviceManagerDao;
//
//    @Autowired
//    private DeviceManage manage;
//
//    @Value("${file.picPath}")
//    private String picPath;
//
//    @Autowired
//    private NPDeviceDao npDeviceDao;
//
//    @Override
//    public void InitialInterface() {
//        DeviceManage manage = new DeviceManage();
//
//        manage.addDevice("192.168.0.20", 37777, "admin", "admin123");
//        manage.addDevice("192.168.0.20", 37777, "admin", "admin123");
//        manage.run();
//        Scanner scanner = new Scanner(System.in);
//        while(true){
//            printTip();
//            int value = scanner.nextInt();
//            if(value == 0){
//                manage.queryDeviceState();
//            }else if(value == 1){
//                manage.queryAttribute();
//            }else if(value == 2){
//                System.out.println("输入阈值: 0~100");
//                int threshold = scanner.nextInt();
////                manage.setAttribute(threshold);
//            }else if(value == 3){
//                manage.pushPicture();
//            }else if(value == 4){
//                break;
//            }
//        }
//        manage.finish();
//    }
//
//    private Integer status = 1;
//
//    public void init(){
//
//        List<String> NPDeviceList = npDeviceDao.ListAllDevice(status);
//
//        String total = NPDeviceList.get(0);
//        for (int i = 1; i <= Integer.parseInt(total); i++) {
//            manage.addDevice(NPDeviceList.get(i), 37777, "admin", "admin123");
//        }
//        manage.run();
//    }
//
//    @Override
//    public void viewDeviceStatus() {
//        manage.queryDeviceState();
//    }
//
//    @Override
//    public List<Map<String,Object>> queryThreshold() {
//        List<Map<String,Object>> res = manage.queryAttribute();
//        return res;
//    }
//
//    //功能可实现，但是时间不稳定
//    @Override
//    public void setThreshold(List<ThresholdVO> thresholdVOList) {
//        //todo 目前功能正常，等后台管理系统的登录功能写完之后再测试
//        for (ThresholdVO thresholdVO : thresholdVOList) {
//            String type = thresholdVO.getType();
//            EM_INSIDE_OBJECT_TYPE objectType = EM_INSIDE_OBJECT_TYPE.getEnum(Integer.valueOf(type));
//            Integer threshold = thresholdVO.getThreshold();//要修改的阈值
//            int value = objectType.getValue();//物品的type
//            String note = objectType.getNote();//物品的名字
////            String username = ShiroUtils.getUserEntity().getUsername();
//            String deviceIP = thresholdVO.getDeviceIP();
//            Integer enable = thresholdVO.getEnable();
//            //将要修改的阈值记录在数据库中 todo 还需要获取到当前登录人的信息
//            deviceManagerDao.updateThreshold(value,note,threshold,"root",deviceIP,enable);
//            deviceManagerDao.insertThresholdForOld(value,note,threshold,"root",deviceIP,enable);
//        }
//        manage.setAttribute(thresholdVOList);
//    }
//
//    List<DeviceClient> deviceClients=new ArrayList<>();
//
//    @Override
//    public void pushPictures(MultipartFile file) {
//        manage.newPushPicture(file);
//    }
//
//    @Override
//    public void exit() {
//        manage.finish();
//    }
//
//    @Override
//    public void pushPicture(JsonObjectVO jsonObjectVO) {
//        manage.newPushPicture(jsonObjectVO);
//    }
//
//    @Override
//    public void savePicture(MultipartFile multipartFiles) {
//        try {
//            //将图片存储到本地指定地址
//            String originalName = multipartFiles.getOriginalFilename();
//            String newName = UUID.randomUUID().toString().replaceAll("-","") + originalName;
//            File file = new File(picPath + newName);
//            if (!file.getParentFile().exists()) file.getParentFile().mkdirs();
//            byte[] bytes = multipartFiles.getBytes();
//            for (int k=0 ; k < bytes.length; k++){
//                if (bytes[k] < 0){
//                    bytes[k] += 256;
//                }
//            }
//            FileOutputStream fileOutputStream = new FileOutputStream(file);
//            fileOutputStream.write(bytes);
//            fileOutputStream.flush();
//            fileOutputStream.close();
//
//            String base64 = Base64.getEncoder().encodeToString(bytes);
////                deviceManagerDao.savePicture(base64);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public Integer savePicture(JsonObjectVO jsonObjectVO) {
//        try {
//            Integer integer = null;
//            //将图片存储到本地指定地址
//
//            BASE64Decoder base64Decoder = new BASE64Decoder();
//            //将图片存储到本地指定地址
//            String name = jsonObjectVO.getName();
//            String device_sn = jsonObjectVO.getDevice_sn();
//            String line_code = jsonObjectVO.getLine_code();
//            Long time = jsonObjectVO.getTime();
//                /*//后缀名
//                String endName = name.substring(name.lastIndexOf("."));
//                //文件名字
//                String Name = name.substring(name.lastIndexOf("/")+1,name.lastIndexOf("."));
//                //防止重复的随机名
//                String newName = UUID.randomUUID().toString().replaceAll("-","") + Name + endName;*/
//
//            String imgBase64Side = jsonObjectVO.getImg_base64_side();
//
//            SimpleDateFormat currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String currentTimeFormat = currentTime.format(new Date());
//            //按照自己的规则定义的图片名字
//            String picName = device_sn.toString() + "@#" + line_code.toString() + "@#" + time + ".jpg";
//            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
//            String date = df.format(new Date());//20230215
//            //固定的储存路径
//            String filePath = picPath + "/" + device_sn.toString() + line_code.toString() + "/" + date + "/";
////                String filePath = testFilePath + "/" + device_sn.toString() + line_code.toString() + "/" + date + "/";
//
//            LogUtils.info("photo info is " + picName);
//            LogUtils.info("photo path is " + filePath + picName);
//
//            File file = new File(filePath + picName);
//            if (!file.getParentFile().exists()) file.getParentFile().mkdirs();
//            byte[] bytes = base64Decoder.decodeBuffer(imgBase64Side);
//            for (int k=0 ; k < bytes.length; k++){
//                if (bytes[k] < 0){
//                    bytes[k] += 256;
//                }
//            }
//            FileOutputStream fileOutputStream = new FileOutputStream(file);
//            fileOutputStream.write(bytes);
//            fileOutputStream.flush();
//            fileOutputStream.close();
//
//            //将图片信息存到数据库中
//            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
//            integer = deviceManagerDao.savePicture(uuid,picName, device_sn, line_code, time, currentTimeFormat);
//            return integer;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    static void printTip(){
//        System.out.println("");
//        System.out.println("0 查看设备状态");
//        System.out.println("1 查询阈值");
//        System.out.println("2 设置阈值");
//        System.out.println("3 推送图片");
//        System.out.println("4 退出");
//    }
//
//    public static void main(String[] args) {
//        try {
//            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");//设置日期格式
//            System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
//            String dateString = df.format(new Date());
//            Date date = df.parse(dateString);
//            System.out.println(date.toString());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//}
