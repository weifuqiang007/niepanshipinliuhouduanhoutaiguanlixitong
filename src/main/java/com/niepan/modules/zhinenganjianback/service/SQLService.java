package com.niepan.modules.zhinenganjianback.service;

import com.niepan.modules.zhinenganjianback.model.AreaVO;
import com.niepan.modules.zhinenganjianback.model.BalanceWheelVO;
import com.niepan.modules.zhinenganjianback.model.NPDeviceRegister;
import com.niepan.modules.zhinenganjianback.model.RequestUrlVO;

import java.util.List;
import java.util.Map;

/**
 * @author: liuchenyu
 * @date: 2023/6/25
 */
public interface SQLService {
    Integer getTableInformation(String table);

    List<Map<String,Object>> getDevice();

    Integer deleteDevice();

    Integer insertDevice(NPDeviceRegister npDeviceRegister);

    List<Map<String, Object>> getArea();

    Integer deleteArea();

    Integer insertArea(AreaVO areaVO);

    List<Map<String, Object>> getRequestUrl();

    Integer deleteRequestUrl();

    Integer insertRequestUrl(RequestUrlVO requestUrlVO);

    List<Map<String, Object>> getBalanceWheel();

    Integer deleteBalanceWheel();

    Integer insertBalanceWheel(BalanceWheelVO balanceWheelVO);
}
