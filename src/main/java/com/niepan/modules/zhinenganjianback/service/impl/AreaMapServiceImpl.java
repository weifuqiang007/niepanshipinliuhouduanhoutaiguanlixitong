package com.niepan.modules.zhinenganjianback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.niepan.modules.zhinenganjianback.dao.AreaMapMapper;
import com.niepan.modules.zhinenganjianback.dto.AreaMap;
import com.niepan.modules.zhinenganjianback.service.AreaMapService;
import org.springframework.stereotype.Service;

/**
* @author zq4568
* @description 针对表【area_map】的数据库操作Service实现
* @createDate 2023-08-18 15:19:28
*/
@Service
public class AreaMapServiceImpl extends ServiceImpl<AreaMapMapper, AreaMap>
    implements AreaMapService {

}

