package com.niepan.modules.zhinenganjianback.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.niepan.modules.zhinenganjianback.dto.AreaDwscode;

/**
* @author zq4568
* @description 针对表【area_dwscode】的数据库操作Service
* @createDate 2023-08-16 17:12:33
*/
public interface AreaDwscodeService extends IService<AreaDwscode> {

    String  selectByDwsCode(String dwsCode);
}
