package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

@Data
public class UpgradeVO {
    String ip;
    String path;
}
