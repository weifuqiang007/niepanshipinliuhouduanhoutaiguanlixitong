package com.niepan.modules.zhinenganjianback.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 智能服务器图像识别返回记录
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("image_identification_records")
public class ImageIdentificationRecords {

    /**
     * 主键id
     */
    @TableId(value = "id",type = IdType.ASSIGN_UUID)
    private String id;

    /**
     *  通道号
     */
    @TableField("channel_id")
    private Integer	nChannelId;

    /**
     *  时间戳(单位是毫秒)
     */
    @TableField("timestamp")
    private Long timestamp;

    /**
     *  创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     *  事件发生的时间
     */
    @TableField("scan_time")
    private Date scanTime;
//    private Long scanTime;

    /**
     *  x光检测的时间
     */
    @TableField("sendmsgstoptime")
    private Date sendmsgstoptime;

    /**
     *  调用中通接口的时间
     */
    @TableField("sendmsgtime")
    private Date sendmsgtime;

    /**
     *  事件ID
     */
    @TableField("n_event_id")
    private Integer nEventId;

    /**
     * 事件名称
     */
    @TableField("event_type")
    private String eventType;
    /**
     *  所属大类
     */
    @TableField("em_class_type")
    private StringBuffer emClassType;

    /**
     *  包裹危险等级, 一个包裹内有多个危险等级显示最高危等级
     */
    @TableField("em_danger_grade")
    private Integer emDangerGrade;

    /**
     * 主视角包裹内物品个数
     */
    @TableField("n_object_num")
    private Integer nObjectNum;

    /**
     * 图片数量
     */
    @TableField("n_image_count")
    private Integer nImageCount;

    /**
     *设备序列号
     */
    @TableField("device_serial_number")
    private String deviceSerialNumber;

    /**
     * 设备ip
     */
    @TableField("device_ip")
    private String deviceIp;

    /**
     * 图片ID
     */
    @TableField("image_id")
    private String imageId;

    private String snCode;
}