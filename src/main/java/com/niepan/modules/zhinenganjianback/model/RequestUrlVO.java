package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

/**
 * @author: liuchenyu
 * @date: 2023/6/25
 */
@Data
public class RequestUrlVO {
    private Integer id;

    /**
     * 上传的接口的ip
     */
    private String requesturl;

    /**
     * minio的ak
     */
    private String access_key;

    /**
     * minio的sk
     */
    private String secret_key;

    /**
     * 地区编码
     */
    private String area_code;

    /**
     * minio的ip
     */
    private String gkj_ip;
}
