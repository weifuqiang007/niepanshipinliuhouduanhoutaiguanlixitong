package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

/**
 * @author: liuchenyu
 * @date: 2023/6/26
 */
@Data
public class BalanceWheelVO {
    private Integer id;

    /**
     * 转动方向
     */
    private String swing;

    /**
     * 转动时间
     */
    private Integer swing_time;

    /**
     * 回正时间
     */
    private Integer correct_time;

    /**
     * 是否开启
     */
    private Integer enable;
    
}
