package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

/**
 * @author weifuqiang
 * @date 2023/2/16
 */

// 安检机心跳
@Data
public class SecurityMachineHeart {

    /**
     * 安检机设备序列号
     */
    String device_sn;

    /**
     *  生产线号(1-18)
     */
    String line_code;

    /**
     * 安检机当前指令状态，0在线，1前进，2后退，3暂停，4其它
     */
    String status_code;
}
