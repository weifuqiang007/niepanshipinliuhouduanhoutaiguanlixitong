package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

@Data
public class AlgorithmVo {
    private Integer id;
    private String name;
    private String type;
    private String remake;
    private String createTime;
    private String status;
    private String path;
}
