package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

/**
 * @author: liuchenyu
 * @date: 2023/6/25
 */
@Data
public class AreaVO {
    /**
     * 地区编码
     */
    private String unique_code;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 中心
     */
    private String centre;
}
