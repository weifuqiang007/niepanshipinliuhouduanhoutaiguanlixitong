package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

@Data
public class SecurityMachinePicture {

    /**
     * 主键id
     */
    Integer id;

    /**
     * 图片名称
     */
    String name;

    /**
     * 安检机设备序列号
     */
    String device_sn;

    /**
     * 生产线号
     */
    String line_code;

    /**
     * 安检机传递时间戳
     */
    String time;

    /**
     * 安检机传来的时间
     */
    String ajjTime;

    /**
     * 存储图片时间
     */
    String currentTime;

    /**
     * 智能安检服务器ip
     */
    String deviceIp;


    /**
     * 图片唯一id
     */
    String pictureId;


    /**
     * 地区编码
     */
    String areaCode;

}
