package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;

/**
 * @author weifuqiang
 * @create_time 2022/12/19
 */


// 安检机状态和故障信息上报表
@Data
public class SecurityMachineStatus {

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 安检机设备序列号
     */
    private String device_sn;

    /**
     * 生产线号(1-18)
     */
    private String line_code;

    /**
     * 故障状态码
     */
    private String  status_code;

    /**
     * 故障信息描述
     */
    private String status_desc;

    /**
     * 故障级别，0正常1通知2警告3故障…
     */
    private String fault_level;

    /**
     * 状态故障采集时间戳，ms
     */
    private Long time;

    /**
     * 当前时间
     */
    private String msgtime;



}
