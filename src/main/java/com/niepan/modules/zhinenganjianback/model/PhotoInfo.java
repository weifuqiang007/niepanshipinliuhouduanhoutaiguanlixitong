package com.niepan.modules.zhinenganjianback.model;

import lombok.Data;
import org.apache.tomcat.jni.Time;

import java.util.Date;

/**
 * @author weifuqiang
 * @create_time 2022/12/18
 */

@Data
public class PhotoInfo {
    /**
     * 安检机设备序列号,需提供可配置入口
     */
    private String device_sn;

    /**
     * 生产线号(1-18) ,需提供可配置入口
     */
    private String line_code;

    /**
     * 侧视角 base64 图片,文件大小<2M（不传头信息）
     */

    private String img_base64_sid;

    /**
     * 数据采集时间
     */
    private Date phototime;

    /**
     * 主键id
     */
    private Integer id;
}
