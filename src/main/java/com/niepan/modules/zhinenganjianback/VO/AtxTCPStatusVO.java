package com.niepan.modules.zhinenganjianback.VO;

import lombok.Data;

@Data
public class AtxTCPStatusVO {
    /**
     * 设备ID
     */
    private String DeviceId;
    /**
     * 设备状态   0 下线 1上线 2 故障
     */
    private Integer DeviceStatus;
    /**
     * 错误类型
     *      0  无
     *      6100 未能与主控制板建立连接
     *      6102 急停开关触发
     *      6104 外罩板微动开关触发
     *      6105 采集系统故障
     */
    private Integer ErrorType;

    /**
     * 状态发送时间   yyyy-MM-dd HH:mm:ss SSS
     */
    private String ErrorTime;
}
