package com.niepan.modules.zhinenganjianback.VO;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ThresholdVO {

    /**
     * 物品的类型
     */
    private String type;

    /**
     * 物品的阈值
     */
    private Integer threshold;

    /**
     * 安检机的IP
     */
    private String deviceIP;

    /**
     * 该违禁品检测是否开启
     */
    private Integer enable;

}
