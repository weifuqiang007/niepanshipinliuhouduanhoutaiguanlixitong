package com.niepan.modules.zhinenganjianback.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class YwtkajVO {

    /**
    图片文件
     */
    private MultipartFile img;

    /**
    厂家设备 SN 码
     */
    private String snCode;

    /**
    DWS 设备 SN 码
     */
    private String dwsCode;
}
