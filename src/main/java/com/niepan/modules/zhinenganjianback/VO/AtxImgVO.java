package com.niepan.modules.zhinenganjianback.VO;

import lombok.Data;

/**
 * @author: liuchenyu
 * @date: 2023/5/23
 */
@Data
public class AtxImgVO {

    /**
     * 设备ID
     */
    private String deviceid;

    /**
     * 检测时间 yyyy-MM-dd HH:mm:ss SSS
     */
    private String checktime;

    /**
     *主视角图片数据 base64
     */
    private String topimagedata;
}
