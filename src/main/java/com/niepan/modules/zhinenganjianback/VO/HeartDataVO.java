package com.niepan.modules.zhinenganjianback.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author: liuchenyu
 * @date: 2023/8/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HeartDataVO {

    /**
     * 地区唯一编码
     */
    @NotBlank(message = "地区唯一编码不能为空")
    private String areaCode;

    /**
     * 心跳的时间戳
     */
    @NotNull(message = "当前日期时间戳不能为空")
    private Long heartbeatTime;

    private String heartbeatTimeString;

    /**
     * 网点名称
     */
    @NotBlank(message = "网点名称不能为空")
    private String siteName;

    /**
     * 动态秤编码
     */
    @NotBlank(message = "动态称的SN码不能为空")
    private String dwsCode;

    /**
     * 智能判图服务器SN码
     */
    @NotBlank(message = "智能判图服务器SN码不能为空")
    private String snCode;

    private Integer heartNum;


    /**
     * 扩展字段
     */
    private Object extendedField;
}
