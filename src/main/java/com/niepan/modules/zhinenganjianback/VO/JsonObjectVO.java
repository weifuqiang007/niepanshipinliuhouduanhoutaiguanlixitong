package com.niepan.modules.zhinenganjianback.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JsonObjectVO implements Serializable {

    /**
     * 文件路径及名字
     */
    private String name;

    /**
     * 大华设备
     */
    private String device_sn;

    /**
     * 安检机唯一code
     */
    private String line_code;

    /**
     * 时间戳
     */
    private Long time;

    /**
     * 存储图片时间
     */
    String currentTime;

    /**
     * base64码
     */
    private String img_base64_side;
}
