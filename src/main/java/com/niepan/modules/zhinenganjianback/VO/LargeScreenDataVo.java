package com.niepan.modules.zhinenganjianback.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LargeScreenDataVo {
    @NotBlank(message = "data_digest不能为空")
    private String data_digest;
    @Valid
    private DataVo  data;
    @NotBlank(message = "msg_type不能为空")
    private String msg_type;
}
