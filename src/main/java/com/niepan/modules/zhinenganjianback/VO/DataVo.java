package com.niepan.modules.zhinenganjianback.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataVo {

    /**
     * 地区唯一编码
     */
    @NotBlank(message = "地区唯一编码不能为空")
    private String areaCode;
    /**
     * 转运中心
     */
    @NotBlank(message = "转运中心不能为空")
    private String centre;

    @NotBlank(message = "companyId不能为空")
    private String companyId;


    @NotBlank(message = "动态称的SN码不能为空")
    private String dwsCode;

    private Object extendedField;

    private Integer forbidCode;

    @NotEmpty(message = "forbidId 不能为空")
    private String forbidId;

    private String forbidType;

    @NotNull(message = "是否是违禁品不能为空")
    private Boolean isForbid;

    @NotNull(message = "当前日期时间戳不能为空")
    private Long scanTime;
    /**
     * 网点名称
     */
    @NotBlank(message = "网点名称不能为空")
    private String siteName;

    @NotBlank(message = "智能判图服务器SN码不能为空")
    private String snCode;

}
