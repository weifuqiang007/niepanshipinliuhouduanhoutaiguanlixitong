package com.niepan.modules.zhinenganjianback.VO;

import lombok.Data;

@Data
public class AtxTCPImgVO {

    /**
     * 设备ID
     */
    private String DeviceId;

    /**
     * 检测时间 yyyy-MM-dd HH:mm:ss SSS
     */
    private String CheckTime;

    /**
     *主视角图片数据 base64
     */
    private String TopImageData;
}
