package com.niepan.modules.sys.service;

import com.niepan.modules.sys.entity.RoleEntity;

import java.util.List;
import java.util.Map;

public interface RoleService {
    List<Map<String, Object>> list();

    Integer addRole(RoleEntity roleEntity);

    List<Map<String, Object>> roleDetail(String roleId);

    Integer updateRole(RoleEntity roleEntity);

    Integer deleteRole(RoleEntity roleEntity);

    List<Map<String, Object>> getRole();
}
