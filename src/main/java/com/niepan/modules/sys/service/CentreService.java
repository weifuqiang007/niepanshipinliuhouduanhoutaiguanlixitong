package com.niepan.modules.sys.service;

import com.niepan.modules.sys.entity.CentreEntity;

import java.util.List;
import java.util.Map;

public interface CentreService {
    List<Map<String, Object>> list(String uniqueCode);

    Integer addCentre(CentreEntity centreEntity);

    List<Map<String, Object>> centreDetail(String id);

    Integer updateCentre(CentreEntity centreEntity);

    Integer deleteCentre(String id);
}
