package com.niepan.modules.sys.service.impl;

import com.niepan.modules.sys.dao.UserDao;
import com.niepan.modules.sys.entity.UserEntity;
import com.niepan.modules.sys.service.UserService;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public List<Map<String, Object>> list() {
        List<Map<String, Object>> list = userDao.list();
        return list;
    }

    @Override
    public Integer addUser(UserEntity userEntity) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        userEntity.setUser_id(uuid);
        userEntity.setPassword(new Sha256Hash(userEntity.getPassword(), userEntity.getSalt()).toHex());
        Integer res = userDao.addUser(userEntity);
        userDao.addUserRole(uuid,userEntity.getRole_id());
        return res;
    }

    @Override
    public List<Map<String,Object>> userDetail(String userId) {
        List<Map<String,Object>> userEntity = userDao.userDetail(userId);
        return userEntity;
    }

    @Override
    public Integer updateUser(UserEntity userEntity) {
        userEntity.setPassword(new Sha256Hash(userEntity.getPassword(), userEntity.getSalt()).toHex());
        Integer res = userDao.updateUser(userEntity);
        userDao.updateUserRole(userEntity.getUser_id(),userEntity.getRole_id());
        return res;
    }

    @Override
    public Integer deleteUser(String userId) {
        Integer res = userDao.deleteUser(userId);
        userDao.deleteUserRole(userId);
        return res;
    }
}
