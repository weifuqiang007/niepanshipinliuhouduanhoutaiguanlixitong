package com.niepan.modules.sys.service.impl;

import com.niepan.modules.sys.dao.RoleDao;
import com.niepan.modules.sys.entity.RoleEntity;
import com.niepan.modules.sys.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleDao roleDao;

    @Override
    public List<Map<String, Object>> list() {
        List<Map<String, Object>> list = roleDao.list();
        return list;
    }

    @Override
    public Integer addRole(RoleEntity roleEntity) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        roleEntity.setRole_id(uuid);
        Integer res = roleDao.addRole(roleEntity);
        String[] menu_id = roleEntity.getMenuList();
        for (String munuid : menu_id) {
            roleDao.addRoleMenu(uuid,munuid);
        }
        return res;
    }

    @Override
    public List<Map<String, Object>> roleDetail(String roleId) {
        List<Map<String,Object>> roleEntity = roleDao.roleDetail(roleId);
        List<Integer> menuId = roleDao.getRoleMenu(roleId);
        for (Map<String, Object> stringObjectMap : roleEntity) {
            stringObjectMap.put("menuList",menuId);
        }
        return roleEntity;
    }

    @Override
    public Integer updateRole(RoleEntity roleEntity) {
        Integer res = roleDao.updateRole(roleEntity);
        String[] menu_id = roleEntity.getMenuList();
        roleDao.deleteRoleMenu(roleEntity.getRole_id());
        for (String munuid : menu_id) {
            roleDao.insertRoleMenu(munuid,roleEntity.getRole_id());
        }
        return res;
    }

    @Override
    public Integer deleteRole(RoleEntity roleEntity) {
        Integer res = roleDao.deleteRole(roleEntity.getRole_id());
        roleDao.deleteRoleMenu(roleEntity.getRole_id());
        return res;
    }

    @Override
    public List<Map<String, Object>> getRole() {
        List<Map<String, Object>> list = roleDao.getRole();
        return list;
    }
}
