package com.niepan.modules.sys.service;

import com.niepan.modules.sys.entity.UserEntity;

import java.util.List;
import java.util.Map;

public interface UserService {
    /**
     * 展示所有的用户
     * @return
     */
    List<Map<String, Object>> list();

    /**
     * 添加用户
     * @param userEntity
     * @return
     */
    Integer addUser(UserEntity userEntity);

    /**
     * 回显用户的信息
     * @param userId
     * @return
     */
    List<Map<String,Object>> userDetail(String userId);

    /**
     * 更新用户信息
     * @param userEntity
     * @return
     */
    Integer updateUser(UserEntity userEntity);

    /**
     * 删除用户
     * @param userId
     * @return
     */
    Integer deleteUser(String userId);
}
