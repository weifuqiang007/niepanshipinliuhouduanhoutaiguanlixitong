package com.niepan.modules.sys.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.niepan.common.utils.R.ApiResponse;
import com.niepan.common.utils.R.Result;
import com.niepan.modules.sys.entity.RoleEntity;
import com.niepan.modules.sys.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    RoleService roleService;

    /**
     * 展示所有的角色
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping("/list")
    public PageInfo list(@RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                         @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum){
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String,Object>> res = roleService.list();
        return new PageInfo<>(res);
    }

    /**
     * 添加角色
     * @param roleEntity
     * @return
     */
    @PostMapping("/addRole")
    public ApiResponse addRole(@RequestBody RoleEntity roleEntity){
        Integer res = roleService.addRole(roleEntity);
        return Result.success(res);
    }

    /**
     * 角色信息回显
     * @param roleId
     * @return
     */
    @GetMapping("/roleDetail")
    public ApiResponse roleDetail(@RequestParam("role_id") String roleId){
        List<Map<String,Object>> res = roleService.roleDetail(roleId);
        return Result.success(res);
    }

    /**
     * 更新角色信息
     * @param roleEntity
     * @return
     */
    @PostMapping("/updateRole")
    public ApiResponse updateRole(@RequestBody RoleEntity roleEntity){
        Integer res = roleService.updateRole(roleEntity);
        return Result.success(res);
    }

    /**
     * 删除角色
     * @param roleEntity
     * @return
     */
    @PostMapping("/deleteRole")
    public ApiResponse deleteRole(@RequestBody RoleEntity roleEntity){
        Integer res = roleService.deleteRole(roleEntity);
        return Result.success(res);
    }

    /**
     * 不分页获取角色信息
     * @return
     */
    @GetMapping("/getRole")
    public ApiResponse getRole(){
        List<Map<String,Object>> res =roleService.getRole();
        return Result.success(res);
    }
}
