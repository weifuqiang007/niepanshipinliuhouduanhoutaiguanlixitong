package com.niepan.modules.sys.entity;

import lombok.Data;

@Data
public class UserEntity {
    /**
     * 用户id
     */
    String user_id;

    /**
     * 用户名
     */
    String username;

    /**
     * 用户昵称
     */
    String nickname;

    /**
     * 用户密码
     */
    String password;

    /**
     * 用户所属地区
     */
    String area;

    /**
     * 用户所属中心
     */
    String center;

    /**
     * 盐
     */
    String salt;

    /**
     * 用户邮箱
     */
    String email;

    /**
     * 用户所属的角色
     */
    String role_id;

    /**
     * 用户手机号
     */
    String mobile;

    /**
     * 用户状态 默认1  删除则为0
     */
    Integer status;

    /**
     * 创建用户的id
     */
    Long create_user_id;

    /**
     * 创建时间
     */
    String create_time;
}
