/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.niepan.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.niepan.modules.sys.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 菜单管理
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysMenuDao extends BaseMapper<SysMenuEntity> {

    List<SysMenuEntity> MenuList() ;

    /**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	List<SysMenuEntity> queryListParentId(Long parentId);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	List<SysMenuEntity> queryNotButtonList();


	List<SysMenuEntity> MenuListByParentId(@Param("menuId") Long menuId);

    List<Map<String, Object>> getMenu(@Param("username") String username);

    List<Map<String, Object>> getMenuByUsername(@Param("username") String username);

    List<Map<String, Object>> getMenuBuParent(@Param("menu_id") Long menu_id);
}
