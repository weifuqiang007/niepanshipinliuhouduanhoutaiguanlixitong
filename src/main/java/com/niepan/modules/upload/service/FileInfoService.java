package com.niepan.modules.upload.service;

import com.niepan.modules.upload.model.QueryInfo;
import com.niepan.modules.upload.model.TFileInfo;

import java.util.List;

public interface FileInfoService {
	
	public int addFileInfo(TFileInfo fileInfo);
	
	public List<TFileInfo> selectFileByParams(TFileInfo fileInfo);
	
	 /**
     * 查询
     *
     * @param query 查询条件
     * @return List
     */
    List<TFileInfo> selectFileList(QueryInfo query);
                    
    
    /**
     * 删除
     * @param tFileInfo
     * @return
     */
    int deleteFile(TFileInfo tFileInfo);
}
