package com.niepan.common.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import org.springframework.stereotype.Component;

/**
 * 将城市名转换为拼音
 */
@Component
public class PinyinUtils {
    public String convertToPinyin(String cityName) {
        StringBuilder pinyin = new StringBuilder();
        for (char ch : cityName.toCharArray()) {
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(ch);
            if (pinyinArray != null && pinyinArray.length > 0) {
                pinyin.append(pinyinArray[0]);
            } else {
                pinyin.append(ch);
            }
        }

        return pinyin.toString();
    }
}
