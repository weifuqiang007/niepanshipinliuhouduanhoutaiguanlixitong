package com.niepan.common.utils.crosorigin;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author weifuqiang
 * @date 2023/3/14
 * 配置跨域信息
 * 返回头中设置“Access-Control-Allow-Origin”参数即可解决跨域问题，此参数就是用来表示允许跨域访问的原始域名的，当设置为“*”时，表示允许所有站点跨域访问
 */

@Configuration
public class CrosConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // 所有接口
                .allowCredentials(true) // 是否发送cookies
                .allowedOriginPatterns("*") // 支持域
                .allowedMethods(new String[]{"GET","POST"}) // 支持方法
                .allowedHeaders("*")
                .exposedHeaders("*");
    }
}
