package com.niepan.common.utils.R;

import lombok.Data;

/**
 * @author weifuqiang
 * @create_time 2022/11/26
 */
@Data
public class ApiResponse<T> {
    /**
     * 系统编码
     */
    int code;

    /**
     * 标准信息
     */
    String msg;

    /**
     * 自定义数据
     */
    T data;
}
