//package com.niepan.common.utils.heart;
//
//import cn.hutool.crypto.digest.DigestUtil;
//import cn.hutool.http.Header;
//import cn.hutool.http.HttpRequest;
//import com.alibaba.fastjson.JSONObject;
//import com.niepan.common.utils.SpringUtil.SpringUtil;
//import com.niepan.common.utils.logs.LogUtils;
//import com.niepan.modules.zhinenganjianback.dao.HeartBeatDao;
//import com.niepan.modules.zhinenganjianback.dao.SecurityMachineDao;
//import com.niepan.modules.zhinenganjianback.demo.DeviceManage;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.ApplicationContext;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Controller;
//
//import java.util.*;
//
///**
// * @author: liuchenyu
// * @date: 2023/5/30
// */
//@Controller
//public class HeartInfo {
//
//    public static List<String> ips = new ArrayList<>();
//
//    @Value("${external.imageCheckServer.requestSecretKey}")
//    private String requestSecretKey;
//
//    @Value("${external.imageCheckServer.msgHeart}")
//    private String msgHeart;
//
//    @Value("${external.imageCheckServer.requestCompanyId}")
//    private String companyId;
//
//    @Autowired
//    private HeartBeatDao heartBeatDao;
//
//    private List<Map<String,Object>> list = new ArrayList<>();
//
//    private Map<String,Object> snAnddwsCode = new HashMap<>();
//
//    public Integer sendHeart(){
//        for (String ip : ips) {
//            ApplicationContext applicationContext = SpringUtil.getApplicationContext();
//            HeartBeatDao bean = applicationContext.getBean(HeartBeatDao.class);
//            snAnddwsCode = bean.getSnAndDwsCode(ip);
//            JSONObject data = new JSONObject();
////        if (list.isEmpty()){
////            list = heartBeatDao.getInfo();
////            for (Map<String, Object> stringObjectMap : list) {
////                snAnddwsCode.put(stringObjectMap.get("sncode").toString(),stringObjectMap.get("dwscode"));
////            }
////        }
//            //厂家设备SN码
//            data.put("snCode", snAnddwsCode.get("sncode"));
//            //DWS设备SN码
//            data.put("dwsCode", snAnddwsCode.get("dwscode"));
//            //安检机图片ID, 目前无法采集到
//            data.put("heartbeatTime", new Date());
//            String json = data.toJSONString();
//            String data_digest = DigestUtil.md5Hex(json+requestSecretKey);
//            Map map = new HashMap<>();
//            map.put("data",json);
//            map.put("data_digest",data_digest);
//            map.put("msg_type",msgHeart);
//            map.put("company_id",companyId);
//            System.out.println("map = " + map);
//
////            String result2 = HttpRequest.post(DeviceManage.url)
////                    .header(Header.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=utf-8")//头信息，多个头信息多次调用此方法即可
////                    .form(map)//表单内容
////                    .timeout(500)//超时，毫秒
////                    .execute().body();
//
////            LogUtils.info("向总部上传心跳 接口返回信息 : " + result2);
//        }
//        return 1;
//    }
//}
