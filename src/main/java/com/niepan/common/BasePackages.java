package com.niepan.common;

/**
 * @author:liuchenyu
 * @data:2023/03/06
 */
public class BasePackages {
    public final static String file = "com.niepan.modules.file.mapper";
    public final static String sys = "com.niepan.modules.sys.dao";
    public final static String app = "com.niepan.modules.app.dao";
    public final static String job = "com.niepan.modules.job.dao";
    public final static String oss = "com.niepan.modules.oss.dao";
    public final static String web = "com.niepan.modules.upload.dao";
    public final static String zhinenganjianback = "com.niepan.modules.zhinenganjianback.dao";

    public final static String all = file + "," + sys + "," + app + "," + job + "," + oss + "," + zhinenganjianback + "," + web;
}
